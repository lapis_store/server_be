"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lapisLog_1 = __importDefault(require("../core/lapisLog"));
const Product_1 = __importDefault(require("../database/models/Product"));
const ProductCache_1 = __importDefault(require("../entities/ProductCache"));
const standardizationKeyword_1 = __importDefault(require("../functions/standardizationKeyword"));
class ProductsCache {
    constructor() {
        this._mapId = new Map();
        this._mapSlug = new Map();
        this._listProducts = [];
        this._suggestions = [];
    }
    get mapId() {
        return this._mapId;
    }
    get mapSlug() {
        return this._mapSlug;
    }
    get listProducts() {
        return this._listProducts;
    }
    get suggestions() {
        return this._suggestions;
    }
    static get isLatest() {
        return this._isLatest;
    }
    static set isLatest(v) {
        this._isLatest = v;
    }
    static async getInstance() {
        if (!this._instance) {
            this._instance = new ProductsCache();
        }
        if (!this.isLatest) {
            await this._instance.update();
        }
        return this._instance;
    }
    async update() {
        const productsDocs = await Product_1.default.find();
        this._mapId = new Map();
        this._mapSlug = new Map();
        this._suggestions = [];
        this._listProducts = [];
        for (const item of productsDocs) {
            if (!item) {
                (0, lapisLog_1.default)('ERROR', 'ProductsCache error with item null || undefined');
                continue;
            }
            const _product = new ProductCache_1.default(item);
            this._mapId.set(_product.id.toString(), _product);
            this._mapSlug.set(_product.data.slug, _product);
            this._suggestions.push({
                key: (0, standardizationKeyword_1.default)(_product.data.title),
                product: _product
            });
            this._listProducts.push(_product);
        }
        ProductsCache.isLatest = true;
    }
}
ProductsCache._instance = undefined;
ProductsCache._isLatest = false;
exports.default = ProductsCache;
