"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const Category_1 = __importDefault(require("../database/models/Category"));
class CategoriesCache {
    constructor() {
        this._data = [];
        this.findOne = (id) => {
            return this._data.find(item => item._id.toString() === id);
        };
        this.getAllChildren = (id) => {
            const getAllChild = (parentId, added = new Set()) => {
                this._data.forEach((item) => {
                    if (item.parentId?.toString() !== parentId)
                        return;
                    if (added.has(item._id.toString()))
                        return;
                    added.add(item._id.toString());
                    getAllChild(item._id.toString(), added);
                });
                return added;
            };
            return getAllChild(id);
        };
        this.update = async () => {
            const categories = await Category_1.default.find();
            this._data = categories.map((item) => {
                return item.toObject();
            });
            CategoriesCache.isLatest = true;
        };
    }
    static get isLatest() {
        return this._isLatest;
    }
    static set isLatest(v) {
        this._isLatest = v;
    }
    get data() {
        return this._data;
    }
}
_a = CategoriesCache;
CategoriesCache._isLatest = false;
CategoriesCache.getInstance = async () => {
    if (!_a._instance)
        _a._instance = new CategoriesCache;
    if (!_a._isLatest) {
        await _a._instance.update();
    }
    return _a._instance;
};
exports.default = CategoriesCache;
