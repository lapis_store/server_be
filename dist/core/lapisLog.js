"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ELogColor = void 0;
var ELogColor;
(function (ELogColor) {
    ELogColor["Reset"] = "\u001B[0m";
    ELogColor["Bright"] = "\u001B[1m";
    ELogColor["Dim"] = "\u001B[2m";
    ELogColor["Underscore"] = "\u001B[4m";
    ELogColor["Blink"] = "\u001B[5m";
    ELogColor["Reverse"] = "\u001B[7m";
    ELogColor["Hidden"] = "\u001B[8m";
    ELogColor["FgBlack"] = "\u001B[30m";
    ELogColor["FgRed"] = "\u001B[31m";
    ELogColor["FgGreen"] = "\u001B[32m";
    ELogColor["FgYellow"] = "\u001B[33m";
    ELogColor["FgBlue"] = "\u001B[34m";
    ELogColor["FgMagenta"] = "\u001B[35m";
    ELogColor["FgCyan"] = "\u001B[36m";
    ELogColor["FgWhite"] = "\u001B[37m";
    ELogColor["BgBlack"] = "\u001B[40m";
    ELogColor["BgRed"] = "\u001B[41m";
    ELogColor["BgGreen"] = "\u001B[42m";
    ELogColor["BgYellow"] = "\u001B[43m";
    ELogColor["BgBlue"] = "\u001B[44m";
    ELogColor["BgMagenta"] = "\u001B[45m";
    ELogColor["BgCyan"] = "\u001B[46m";
    ELogColor["BgWhite"] = "\u001B[47m";
})(ELogColor = exports.ELogColor || (exports.ELogColor = {}));
async function lapisLog(logType, ...message) {
    let color = '';
    switch (logType) {
        case 'ERROR': {
            color = ELogColor.FgRed;
            break;
        }
        case 'INFO': {
            color = ELogColor.FgBlue;
            break;
        }
        case 'SUCCESS': {
            color = ELogColor.FgCyan;
            break;
        }
        case 'WAITTING': {
            color = ELogColor.FgYellow;
            break;
        }
        case 'WARNING': {
            color = ELogColor.FgYellow;
            break;
        }
        default: {
            throw new Error('[lapisLog] Invalid logType');
        }
    }
    console.log(color, `[${logType}]`, ELogColor.Reset, ...message);
}
exports.default = lapisLog;
