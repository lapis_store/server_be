"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RegExpHub {
}
RegExpHub.phoneNumber = /^[0-9]{10}$/i;
RegExpHub.objectId = /^[0-9a-f]{24}$/i;
RegExpHub.date = /^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z$/i;
RegExpHub.integer = /^\d*$/i;
exports.default = RegExpHub;
