"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const lapisLog_1 = __importDefault(require("./lapisLog"));
(0, lapisLog_1.default)('INFO', `NODE_ENV="${process.env.NODE_ENV}"`);
if (process.env.NODE_ENV) {
    dotenv_1.default.config({
        path: `.env.${process.env.NODE_ENV}`,
    });
}
