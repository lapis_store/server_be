"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
class ENV {
}
ENV.DATA_DIR = path_1.default.join(__dirname, '../../public/data');
ENV.SHOP_NAME = process.env.SHOP_NAME || '';
ENV.IS_DEV = (process.env.NODE_ENV && process.env.NODE_ENV === 'development') || false;
ENV.PORT = process.env.PORT
    ? parseInt(process.env.PORT)
    : 8080;
// DATABASE
ENV.DATABASE_HOST = process.env.DATABASE_HOST || '127.0.0.1:27017';
ENV.DATABASE_NAME = process.env.DATABASE_NAME || 'default';
ENV.DATABASE_USERNAME = process.env.DATABASE_USERNAME || '';
ENV.DATABASE_PASSWORD = process.env.DATABASE_PASSWORD || '';
ENV.DIRECT_CONNECTION = String(process.env.DIRECT_CONNECTION) === 'true' || false;
ENV.AUTH_SOURCE = process.env.AUTH_SOURCE || '';
// TOKEN
ENV.ACCESS_TOKEN_SECRET = process.env.ACCESS_TOKEN_SECRET || 'default';
ENV.REFRESH_TOKEN_SECRET = process.env.REFRESH_TOKEN_SECRET || 'default';
// TIME EXPIRE TOKEN
ENV.ACCESS_TOKEN_EXPIRE_IN = process.env.ACCESS_TOKEN_EXPIRE_IN || '15m';
ENV.REFRESH_TOKEN_EXPIRE_IN = process.env.REFRESH_TOKEN_EXPIRE_IN || '15m';
exports.default = ENV;
