"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const objectId = /^[a-z0-9]+$/;
const lapisRegex = {
    objectId,
};
exports.default = lapisRegex;
