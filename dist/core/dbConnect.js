"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const lapisLog_1 = __importDefault(require("./lapisLog"));
const ENV_1 = __importDefault(require("./ENV"));
const makeConnectionString = () => {
    const params = {
        directConnection: String(ENV_1.default.DIRECT_CONNECTION),
        authMechanism: 'DEFAULT',
        authSource: ENV_1.default.AUTH_SOURCE
    };
    const strParams = Object.keys(params).map(key => {
        return [key, params[key]].join('=');
    });
    // mongodb://username:password@db.lapisblog.com:27017/?directConnection=false&authMechanism=DEFAULT&authSource=check
    return `mongodb://${ENV_1.default.DATABASE_USERNAME}:${ENV_1.default.DATABASE_PASSWORD}@${ENV_1.default.DATABASE_HOST}/${ENV_1.default.DATABASE_NAME}?${strParams.join('&')}`;
};
async function default_1() {
    (0, lapisLog_1.default)('WAITTING', `Connecting to ${ENV_1.default.DATABASE_NAME}`);
    const connectionString = makeConnectionString();
    try {
        await mongoose_1.default.connect(connectionString, {
            keepAlive: true,
        });
        (0, lapisLog_1.default)('SUCCESS', `Connect to ${ENV_1.default.DATABASE_NAME} successfully !`);
        return true;
    }
    catch (e) {
        (0, lapisLog_1.default)('ERROR', `Connect to ${ENV_1.default.DATABASE_NAME} failed !`);
    }
    return false;
}
exports.default = default_1;
