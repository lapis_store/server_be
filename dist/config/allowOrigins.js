"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const allowOrigins = [
    {
        protocol: ['http', 'https'],
        domain: 'lapisstore.lapisblog.com',
        ports: [80, 443]
    },
    {
        protocol: ['http', 'https'],
        domain: 'dev-lapisstore.lapisblog.com',
        ports: [80, 443]
    },
    {
        protocol: ['http', 'https'],
        domain: 'admin-lapisstore.lapisblog.com',
        ports: [80, 443]
    },
    {
        protocol: ['http', 'https'],
        domain: 'dev-admin-lapisstore.lapisblog.com',
        ports: [80, 443]
    },
];
exports.default = allowOrigins;
