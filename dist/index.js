"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("./core/configEnv");
const express_1 = __importDefault(require("express"));
const lapisLog_1 = __importStar(require("./core/lapisLog"));
const router_1 = __importDefault(require("./router"));
const ENV_1 = __importDefault(require("./core/ENV"));
const dbConnect_1 = __importDefault(require("./core/dbConnect"));
(async () => {
    const app = (0, express_1.default)();
    if ((await Promise.all([
        (0, dbConnect_1.default)()
    ])).includes(false)) {
        (0, lapisLog_1.default)('ERROR', `Some process failed when startup`);
        return;
    }
    await (0, dbConnect_1.default)();
    (0, router_1.default)(app);
    app.set('trust proxy', 1);
    app.get('/', (req, res) => {
        res.send('ok');
        res.end();
    });
    app.listen(ENV_1.default.PORT, () => {
        (0, lapisLog_1.default)('INFO', `Server running on ${lapisLog_1.ELogColor.FgBlue}http://127.0.0.1:${ENV_1.default.PORT}/${lapisLog_1.ELogColor.Reset}`);
    });
})();
