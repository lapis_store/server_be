"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const EOrderStatus_1 = __importDefault(require("../types/EOrderStatus"));
const OrderSchema = new mongoose_1.default.Schema({
    userId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        default: undefined,
    },
    customerName: {
        type: String,
        required: true,
    },
    orderStatus: {
        type: String,
        default: EOrderStatus_1.default.unconfirmed,
    },
    phoneNumber: {
        type: String,
        validate: {
            validator: (v) => {
                const regexPhoneNumber = /^[0-9]{10}$/;
                return regexPhoneNumber.test(v);
            },
            message: () => {
                return `Invalid phone number`;
            }
        },
        required: true,
    },
    email: {
        type: String,
        default: undefined,
    },
    address: {
        type: String,
        required: true,
    },
    total: {
        type: Number,
        required: true,
    },
    orderItems: [{
            productId: {
                type: mongoose_1.default.Schema.Types.ObjectId,
                required: true,
            },
            productName: {
                type: String,
                required: true,
            },
            price: {
                type: Number,
                validate: {
                    validator: (v) => {
                        return v > 0;
                    },
                    message: (e) => {
                        return `Price can't be less than 0`;
                    }
                },
                required: true,
            },
            quantity: {
                type: Number,
                validate: {
                    validator: (v) => {
                        return v > 0;
                    },
                    message: (e) => {
                        return `Quantity can't be less than 0`;
                    }
                },
                required: true,
            }
        }],
    updatedAt: {
        type: Date,
        default: () => new Date(),
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => new Date(),
    }
});
exports.default = OrderSchema;
