"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Category_1 = __importDefault(require("../models/Category"));
const CategorySchema = new mongoose_1.default.Schema({
    index: {
        type: Number,
        default: 0,
    },
    title: {
        type: String,
        default: '',
    },
    image: {
        type: String,
        default: undefined,
    },
    parentId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: false,
        default: undefined,
        validate: {
            validator: async (v) => {
                return await Category_1.default.idIsExisted(v);
            },
            message: (v) => `Invalid parentId: ${v} not exists. parentId must be undefined or categoryId existed`,
        },
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => new Date(Date.now()),
    },
    updatedAt: {
        type: Date,
        default: () => new Date(Date.now()),
    },
});
exports.default = CategorySchema;
