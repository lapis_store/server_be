"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Category_1 = __importDefault(require("../models/Category"));
const ProductSchema = new mongoose_1.default.Schema({
    title: {
        type: String,
        default: '',
    },
    slug: {
        type: String,
        required: true,
        minlength: 1,
    },
    categoryId: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        default: undefined,
        validate: {
            validator: async (id) => {
                if (!id)
                    return true;
                // check productId is existed in Product table
                const product = await Category_1.default.exists({
                    _id: id,
                });
                if (!product)
                    return false;
                return true;
            },
            message: (e) => {
                return `categoryId="${e.value.toString()}" does not exists in the Category table`;
            },
        },
    },
    summary: {
        type: String,
        default: '',
    },
    price: {
        type: Number,
        min: 0,
        default: 0,
    },
    isPromotionalPrice: {
        type: Boolean,
        default: false,
    },
    promotionPrice: {
        type: Number,
        default: 0,
    },
    promotionStartAt: {
        type: Date,
    },
    promotionEndAt: {
        type: Date,
    },
    image: {
        type: String,
        default: '',
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => {
            return new Date();
        },
    },
    updatedAt: {
        type: Date,
        default: () => {
            return new Date();
        },
    },
});
exports.default = ProductSchema;
