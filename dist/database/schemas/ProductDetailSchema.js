"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Product_1 = __importDefault(require("../models/Product"));
const ProductDetailSchema = new mongoose_1.default.Schema({
    _id: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        validate: {
            validator: async (id) => {
                // check productId is existed in Product table
                const product = await Product_1.default.exists({
                    _id: id,
                });
                if (!product)
                    return false;
                return true;
            },
            message: (e) => {
                return `_id="${e.value.toString()}" does not exists in the Product table`;
            },
        },
    },
    metaTag: [
        {
            name: {
                type: String,
                default: '',
            },
            content: {
                type: String,
                default: '',
            },
        },
    ],
    images: [
        {
            type: String,
        },
    ],
    information: {
        type: String,
        default: '',
    },
    promotion: {
        type: String,
        default: '',
    },
    specifications: [
        {
            name: {
                type: String,
                default: '',
            },
            value: {
                type: String,
                default: '',
            },
        },
    ],
    description: {
        type: String,
        default: '',
    },
    updatedAt: {
        type: Date,
        default: () => new Date(),
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => new Date(),
    },
});
exports.default = ProductDetailSchema;
