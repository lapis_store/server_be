"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EOrderStatus;
(function (EOrderStatus) {
    EOrderStatus["unconfirmed"] = "unconfirmed";
    EOrderStatus["processing"] = "processing";
    EOrderStatus["inTransit"] = "in-transit";
    EOrderStatus["completed"] = "completed";
    EOrderStatus["cancelled"] = "cancelled";
})(EOrderStatus || (EOrderStatus = {}));
exports.default = EOrderStatus;
