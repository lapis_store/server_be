"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const ProductSchema_1 = __importDefault(require("../schemas/ProductSchema"));
// VIRTUAL PROPERTIES
ProductSchema_1.default.virtual('currentPrice').get(function () {
    if (!this.isPromotionalPrice)
        return this.price;
    const currentTime = new Date();
    if (!this.promotionStartAt) {
        if (!this.promotionEndAt)
            return this.promotionPrice;
        if (currentTime < this.promotionEndAt)
            return this.promotionPrice;
        return this.price;
    }
    if (!this.promotionEndAt) {
        if (!this.promotionStartAt)
            return this.promotionPrice;
        if (this.promotionStartAt < currentTime)
            return this.promotionPrice;
        return this.price;
    }
    if (this.promotionStartAt < currentTime && currentTime < this.promotionEndAt) {
        return this.promotionPrice;
    }
    return this.price;
});
ProductSchema_1.default.virtual('isPromotion').get(function () {
    if (!this.isPromotionalPrice)
        return false;
    const currentTime = new Date();
    if (!this.promotionStartAt) {
        if (!this.promotionEndAt)
            return true;
        if (currentTime < this.promotionEndAt)
            return true;
        return false;
    }
    if (!this.promotionEndAt) {
        if (!this.promotionStartAt)
            return true;
        if (currentTime > this.promotionStartAt)
            return true;
        return false;
    }
    if (this.promotionStartAt < currentTime && currentTime < this.promotionEndAt) {
        return true;
    }
    return false;
});
// METHODS
ProductSchema_1.default.methods.slugIsExisted = async function () {
    if (!this.slug)
        return undefined;
    const product = await Product.findBySlug(this.slug);
    if (!product)
        return false;
    if (!this._id)
        return true;
    if (product._id.toString() === this._id.toString()) {
        return false;
    }
    return true;
};
// STATICS
ProductSchema_1.default.statics.findBySlug = async function (slug) {
    return await Product.findOne({
        slug: slug,
    });
};
// EVENT
ProductSchema_1.default.pre('save', async function (next) {
    const currentTime = new Date(Date.now());
    this.updatedAt = currentTime;
    this.createdAt = currentTime;
    next();
});
// VALIDATOR
ProductSchema_1.default
    .path('slug')
    .validate(async function (value) {
    if (!this)
        return;
    const checkSlug = /^[a-z0-9\-]+$/;
    if (!checkSlug.test(this.slug))
        return false;
    return !(await this.slugIsExisted());
}, `Invalid slug: slug include [a-z0-9\\-]{1,}`);
const Product = mongoose_1.default.model('Product', ProductSchema_1.default);
exports.default = Product;
