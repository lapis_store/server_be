"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const lapisLog_1 = __importDefault(require("../../core/lapisLog"));
const CategorySchema_1 = __importDefault(require("../schemas/CategorySchema"));
// VIRTUAL
CategorySchema_1.default
    .virtual('fullPath')
    .get(async function () {
    const ROOT_TITLE = 'LapisStore';
    const getPath = async (id, listParentId = []) => {
        const category = await Category.findById(id);
        if (!category || category === null)
            return '';
        if (!category.parentId)
            return category.title;
        // avoid endless "circular loops"
        const strParentId = category.parentId.toString();
        if (listParentId.includes(strParentId)) {
            (0, lapisLog_1.default)('WARNING', `endless "circular loops", ${JSON.stringify(listParentId)}`);
            return '';
        }
        listParentId.push(strParentId);
        const parentCategoryPath = await getPath(category.parentId, listParentId);
        return `${parentCategoryPath}/${category.title}`;
    };
    if (!this.parentId)
        return '';
    // LapisStore/{parentPath}/title
    return `${ROOT_TITLE}/${await getPath(this.parentId)}/${this.title}`;
});
// METHODS
CategorySchema_1.default.methods.parentIdIsExisted = async function () {
    return await Category.idIsExisted(this.parentId);
};
CategorySchema_1.default.methods.fullPathIsCircularLoops = async function () {
    if (!this._id || !this.parentId)
        return false;
    if (this._id.toString() === this.parentId.toString())
        return true;
    const checkFullPathIsCircularLoops = async (id, listParentId = []) => {
        const category = await Category.findById(id);
        if (!category || category === null)
            return false;
        if (!category.parentId)
            return false;
        // avoid endless "circular loops"
        const strParentId = category.parentId.toString();
        if (listParentId.includes(strParentId)) {
            return true;
        }
        listParentId.push(strParentId);
        return await checkFullPathIsCircularLoops(category.parentId, listParentId);
    };
    return await checkFullPathIsCircularLoops(this.parentId, [this._id.toString()]);
};
// STATICS
CategorySchema_1.default.statics.idIsExisted = async function (id) {
    if (!id)
        return false;
    const parentCategory = await Category.exists({
        _id: id,
    });
    if (parentCategory) {
        return true;
    }
    return false;
};
// EVENT
CategorySchema_1.default.pre('save', async function (next) {
    const currentTime = new Date(Date.now());
    this.createdAt = currentTime;
    this.updatedAt = currentTime;
    next();
});
const Category = mongoose_1.default.model('Category', CategorySchema_1.default);
exports.default = Category;
