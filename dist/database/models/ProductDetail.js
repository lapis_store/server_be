"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const ProductDetailSchema_1 = __importDefault(require("../schemas/ProductDetailSchema"));
ProductDetailSchema_1.default.pre('save', async function (next) {
    const currentTime = new Date(Date.now());
    this.updatedAt = currentTime;
    next();
});
const ProductDetail = mongoose_1.default.model('ProductDetail', ProductDetailSchema_1.default);
exports.default = ProductDetail;
