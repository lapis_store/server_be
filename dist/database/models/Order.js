"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const OrderSchema_1 = __importDefault(require("../schemas/OrderSchema"));
OrderSchema_1.default.pre('save', async function (next) {
    const currentTime = new Date(Date.now());
    this.createdAt = currentTime;
    this.updatedAt = currentTime;
    next();
});
const Order = mongoose_1.default.model('order', OrderSchema_1.default);
exports.default = Order;
