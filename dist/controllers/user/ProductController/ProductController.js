"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const CategoriesCache_1 = __importDefault(require("../../../cache/CategoriesCache"));
const ProductsCache_1 = __importDefault(require("../../../cache/ProductsCache"));
const ProductDetail_1 = __importDefault(require("../../../database/models/ProductDetail"));
class ProductController {
    constructor() {
        this.index = async (req, res, next) => {
            // res.status(200).send('Setup home controller successfully');
        };
        // [GET] /product/list
        this.list = async (req, res) => {
            const categoryId = req.query.categoryId;
            const sortBy = -1;
            let products = (await ProductsCache_1.default.getInstance()).listProducts;
            if (typeof categoryId === 'string' && mongoose_1.default.Types.ObjectId.isValid(categoryId)) {
                const categories = (await CategoriesCache_1.default.getInstance()).getAllChildren(categoryId);
                categories.add(categoryId);
                if (categories.size !== 0) {
                    products = products.filter((product) => {
                        if (!product.data.categoryId)
                            return false;
                        return categories.has(product.data.categoryId.toString());
                    });
                }
            }
            products.sort((a, b) => {
                const aCreatedAt = a.data.createdAt;
                const bCreatedAt = b.data.createdAt;
                if (aCreatedAt > bCreatedAt)
                    return 1 * sortBy;
                if (aCreatedAt < bCreatedAt)
                    return -1 * sortBy;
                return 0;
            });
            return res.status(200).json(products.map(item => item.toResObject()));
        };
        this.latestProducts = async (req, res) => {
            const products = (await ProductsCache_1.default.getInstance()).listProducts.map((item) => item.toResObject());
            return res.status(200).json(products);
        };
        // [GET] /get-by-id/:id
        this.getById = async (req, res, next) => {
            const id = req.params.id && String(req.params.id);
            if (!id)
                return res.status(404).send();
            const product = (await ProductsCache_1.default.getInstance()).mapId.get(id);
            if (!product) {
                return res.status(404).send();
            }
            return res.status(200).json(product.toResObject());
        };
        // [GET] /get-by-list-id
        this.getByListId = async (req, res, next) => {
            const listId = req.query.listId;
            if (!listId || !Array.isArray(listId)) {
                return res.status(404).send();
            }
            const mapProducts = (await ProductsCache_1.default.getInstance()).mapId;
            const resData = [];
            listId.forEach((id) => {
                const _id = String(id);
                const product = mapProducts.get(_id);
                if (!product)
                    return;
                resData.push(product.toResObject());
            });
            return res.status(200).json(resData);
        };
        this.getBySlug = async (req, res, next) => {
            const slug = req.query.slug;
            if (!slug || typeof slug !== 'string') {
                res.status(404).send();
                res.end();
                return;
            }
            const product = (await ProductsCache_1.default.getInstance()).mapSlug.get(slug);
            if (!product) {
                res.status(404).send();
                res.end();
                return;
            }
            const productDetail = await ProductDetail_1.default.findOne({
                _id: new mongoose_1.default.Types.ObjectId(product.id),
            }, {
                _id: 1,
                metaTag: 1,
                images: 1,
                information: 1,
                promotion: 1,
                promotionMore: 1,
                specifications: 1,
                description: 1,
            });
            const resData = product.toResObject();
            return res.status(200).json({
                product: resData,
                productDetail: productDetail && productDetail.toObject()
            });
        };
    }
    static get instance() {
        if (!this._instance)
            this._instance = new ProductController();
        return this._instance;
    }
}
exports.default = ProductController;
