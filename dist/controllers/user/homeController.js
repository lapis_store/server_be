"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class HomeController {
    constructor() {
        this.index = async (req, res, next) => {
            res.status(200).send('Setup home controller successfully');
        };
    }
}
const homeController = new HomeController();
exports.default = homeController;
