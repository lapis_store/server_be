"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const CategoriesCache_1 = __importDefault(require("../../../cache/CategoriesCache"));
class CategoryController {
    constructor() {
        this.index = async (req, res, next) => { };
        // [GET] /list
        this.list = async (req, res, next) => {
            // cache
            // if(this.categoriesCache){
            //     return res.status(200).json(this.categoriesCache);
            // }
            // const categories = await Category.find();
            // const categoriesRes = categories.map((category) => {
            //     return category.toObject();
            // });
            // this.categoriesCache = categoriesRes;
            return res.status(200).json((await CategoriesCache_1.default.getInstance()).data);
        };
        // [GET] /find/:id
        this.find = async (req, res) => {
            const id = req.params.id;
            if (typeof id !== 'string') {
                return res.status(404).send();
            }
            const category = (await CategoriesCache_1.default.getInstance()).data.find(item => item._id.toString() === id);
            if (!category) {
                return res.status(404).send();
            }
            return res.status(200).json(category);
        };
    }
    static get instance() {
        if (!this._instance)
            this._instance = new CategoryController();
        return this._instance;
    }
}
exports.default = CategoryController;
