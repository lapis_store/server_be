"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ProductsCache_1 = __importDefault(require("../../../cache/ProductsCache"));
const lapisLog_1 = __importDefault(require("../../../core/lapisLog"));
const standardizationKeyword_1 = __importDefault(require("../../../functions/standardizationKeyword"));
class SearchController {
    constructor() {
        this.maxSuggestionsRes = 7;
        this.index = async (req, res, next) => {
            const keyword = req.query.keyword && String(req.query.keyword);
            if (!keyword) {
                (0, lapisLog_1.default)('ERROR', 'suggestions with q undefined', keyword);
                res.status(404).send();
                res.end();
                return;
            }
            const keywordReg = new RegExp((0, standardizationKeyword_1.default)(keyword), 'i');
            const suggestions = (await ProductsCache_1.default.getInstance()).suggestions;
            const resData = [];
            for (let item of suggestions) {
                if (keywordReg.test(item.key)) {
                    resData.push(item.product.toResObject());
                }
                if (resData.length >= 40) {
                    break;
                }
            }
            resData.sort((a, b) => {
                const aTitle = a.title.toLowerCase();
                const bTitle = b.title.toLowerCase();
                if (aTitle > bTitle)
                    return 1;
                if (aTitle < bTitle)
                    return -1;
                return 0;
            });
            res.status(200).json(resData);
            res.end();
        };
        // [GET] /suggestions/q=
        this.suggestions = async (req, res, next) => {
            const q = req.query.q && String(req.query.q);
            if (!q) {
                (0, lapisLog_1.default)('ERROR', 'suggestions with q undefined', q);
                res.status(404).send();
                res.end();
                return;
            }
            const keywordReg = new RegExp((0, standardizationKeyword_1.default)(q), 'i');
            const suggestions = (await ProductsCache_1.default.getInstance()).suggestions;
            const suggestionsRes = [];
            for (let item of suggestions) {
                if (keywordReg.test(item.key)) {
                    suggestionsRes.push(item.product.data.title);
                }
                if (suggestionsRes.length >= this.maxSuggestionsRes) {
                    break;
                }
            }
            suggestionsRes.sort();
            res.status(200).json(suggestionsRes);
            res.end();
        };
    }
    static get instance() {
        if (!this._instance)
            this._instance = new SearchController();
        return this._instance;
    }
}
exports.default = SearchController;
