"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const ProductsCache_1 = __importDefault(require("../../../cache/ProductsCache"));
const lapisLog_1 = __importDefault(require("../../../core/lapisLog"));
const RegExpHub_1 = __importDefault(require("../../../core/RegExpHub"));
class BaseOrderController {
    constructor() {
        this.validateFormData = async (formData) => {
            if (!formData)
                return {
                    status: 'invalid',
                    message: 'Invalid form data',
                };
            const { customerName, phoneNumber, address, orderItems, email, } = formData;
            // customer name
            if (typeof customerName !== 'string'
                || customerName.length === 0)
                return {
                    status: 'invalid',
                    message: `Invalid customer name value="${customerName}"`,
                };
            // phone number
            if (typeof phoneNumber !== 'string'
                || !RegExpHub_1.default.phoneNumber.test(phoneNumber))
                return {
                    status: 'invalid',
                    message: 'Invalid phoneNumber',
                };
            // address
            if (typeof address !== 'string'
                || address.length == 0)
                return {
                    status: 'invalid',
                    message: 'Invalid address',
                };
            // order items
            if (!Array.isArray(orderItems)
                || orderItems.length === 0)
                return {
                    status: 'invalid',
                    message: 'Invalid order items',
                };
            for (let { productId, quantity, } of orderItems) {
                if (typeof productId !== 'string'
                    || !RegExpHub_1.default.objectId.test(productId))
                    return {
                        status: 'invalid',
                        message: 'Some item in order are not valid',
                    };
                if (typeof quantity !== 'number'
                    || quantity <= 0)
                    return {
                        status: 'invalid',
                        message: 'Some quantity of item in order are not valid',
                    };
            }
            return {
                status: 'valid',
                message: '',
            };
        };
        this.makeOrderDocument = async (formData) => {
            const { customerName, phoneNumber, address, email, } = formData;
            const mapProduct = (await ProductsCache_1.default.getInstance()).mapId;
            // 
            const orderItems = [];
            for (let item of formData.orderItems) {
                const product = mapProduct.get(item.productId);
                if (!product) {
                    (0, lapisLog_1.default)('WARNING', `Items in orderItems have productId="${item.productId}" not exists in current list product`);
                    return undefined;
                }
                orderItems.push({
                    productId: new mongoose_1.default.Types.ObjectId(item.productId),
                    productName: product.data.title,
                    price: product.currentPrice,
                    quantity: item.quantity
                });
            }
            const total = orderItems.reduce((_total, currentItem) => {
                const { price, quantity, } = currentItem;
                return _total + price * quantity;
            }, 0);
            const orderDocument = {
                customerName,
                phoneNumber,
                address,
                email,
                orderItems,
                total,
            };
            return orderDocument;
        };
    }
}
exports.default = BaseOrderController;
