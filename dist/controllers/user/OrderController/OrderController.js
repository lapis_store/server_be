"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lapisLog_1 = __importDefault(require("../../../core/lapisLog"));
const Order_1 = __importDefault(require("../../../database/models/Order"));
const BaseOrderController_1 = __importDefault(require("./BaseOrderController"));
class OrderController extends BaseOrderController_1.default {
    constructor() {
        super();
        // [POST] /add
        this.add = async (req, res, next) => {
            const formData = req.body;
            if (!formData)
                return res.status(400).send();
            const validResult = await this.validateFormData(formData);
            if (validResult.status === 'invalid') {
                res.status(400).json({
                    message: validResult.message,
                });
                return res.end();
            }
            const orderDocument = await this.makeOrderDocument(formData);
            if (!orderDocument) {
                res.status(400).json({
                    message: 'Create order document failed',
                });
                return res.end();
            }
            try {
                // save
                const order = new Order_1.default(orderDocument);
                await order.save();
                const resOrder = order.toJSON();
                const resData = {
                    _id: resOrder._id.toString(),
                    customerName: resOrder.customerName,
                    phoneNumber: resOrder.phoneNumber,
                    address: resOrder.address,
                    total: resOrder.total,
                    orderItems: resOrder.orderItems.map((item) => {
                        return {
                            productId: item.productId.toString(),
                            productName: item.productName,
                            price: item.price,
                            quantity: item.quantity,
                        };
                    })
                };
                res.status(201).send(resData);
                res.end();
            }
            catch (e) {
                (0, lapisLog_1.default)('ERROR', e.message);
                res.status(500).send();
                res.end();
            }
        };
    }
    static get instance() {
        if (!this._instance)
            this._instance = new OrderController();
        return this._instance;
    }
}
exports.default = OrderController;
