"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
//import generateAccessToken from "../core/generateAccessToken";
const lapisLog_1 = __importDefault(require("../../core/lapisLog"));
const ENV_1 = __importDefault(require("../../core/ENV"));
class LoginController {
    constructor() {
        this.index = async (req, res, next) => {
            res.status(200).send('Setup home controller successfully');
        };
        this.login = async (req, res, next) => {
            const userName = req.query.username && req.query.username;
            (0, lapisLog_1.default)('INFO', req.query);
            (0, lapisLog_1.default)('INFO', req.body);
            res.status(200).json(this.generateAccessToken({
                userName,
            }));
        };
        this.generateAccessToken = (payload) => {
            const accessToken = jsonwebtoken_1.default.sign(payload, ENV_1.default.ACCESS_TOKEN_SECRET, {
                expiresIn: ENV_1.default.ACCESS_TOKEN_EXPIRE_IN,
            });
            const refreshToken = jsonwebtoken_1.default.sign(payload, ENV_1.default.ACCESS_TOKEN_SECRET, {
                expiresIn: ENV_1.default.REFRESH_TOKEN_EXPIRE_IN,
            });
            return {
                accessToken,
                refreshToken,
            };
        };
    }
}
const loginController = new LoginController();
exports.default = loginController;
