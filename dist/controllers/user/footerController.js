"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class FooterController {
    constructor() {
        this.index = async () => { };
        this.list = async (req, res) => {
            const footerItems = [
                {
                    title: 'Giới thiệu công ty',
                    width: 'f50',
                    description: `Công ty Cổ phần Đầu tư Tên Công Ty là nhà bán lẻ số 1 Việt Nam về doanh thu và lợi nhuận,
                            với mạng lưới hơn 10.800 cửa hàng trên toàn quốc. Tên Công Ty vận hành các chuỗi bán lẻ diện thoại,
                            Chuỗi cửa hàng điện máy, Chuỗi siêu thị mini.`,
                    links: [
                        { title: 'Chuỗi bán lẻ diện thoại', url: '#' },
                        { title: 'Chuỗi cửa hàng điện máy', url: '#' },
                        { title: 'Chuỗi siêu thị mini', url: '#' },
                    ],
                },
                {
                    title: 'Tìm cửa hàng',
                    width: 'f25',
                    links: [
                        { title: 'Tìm cửa hàng gần nhất', url: '#' },
                        { title: 'Mua hàng từ xa', url: '#' },
                        { title: 'Chat trực tiếp vối nhân viên để tìm cửa hàng', url: '#' },
                        {
                            title: 'Gặp trực tiếp cửa hàng gần nhất (Zalo hoặc gọi điện)',
                            url: '#',
                        },
                    ],
                },
                {
                    title: 'Tra cứu',
                    width: 'f25',
                    links: [
                        { title: 'Tra thông tin đơn hàng', url: '#' },
                        { title: 'Tra điểm tích luỹ mua hàng', url: '#' },
                        { title: 'Tra thông tin bảo hành', url: '#' },
                    ],
                },
                {
                    title: 'Liên hệ',
                    width: 'f25',
                    links: [
                        { title: 'Gọi mua hàng: 1800.2088 (8h00 - 22h00)', url: '#' },
                        { title: 'Gọi mua hàng: 1700.3077 (8h00 - 22h00)', url: '#' },
                        { title: 'Gọi mua hàng: 1600.4066 (8h00 - 22h00)', url: '#' },
                    ],
                },
                {
                    title: 'Chính sách mua hàng và mạng xã hội',
                    width: 'f75',
                    description: 'Một đổi một trong vòng một tháng đối với sản phẩm mới. (Xem đầy đủ về chính sách ở link bên dưới)',
                    links: [
                        { title: 'Chính sách bảo hành', url: '#' },
                        { title: 'Khiếu nại: 1900.2073 (8:00 - 21:30)', url: '#' },
                        { title: 'Bảo hành: 1900.3084 (8:00 - 21:00)', url: '#' },
                    ],
                    icons: [
                        { name: '&#61570', url: 'https://www.facebook.com/' },
                        { name: '&#61802', url: 'https://www.youtube.com/' },
                        { name: '&#62150', url: 'https://telegram.org/' },
                        { name: '\\f1b9', url: '#' },
                    ],
                },
                {
                    align: 'center',
                    width: 'f100',
                    description: 'Công ty TNHH Thương mại và dịch vụ kỹ thuật Tên Công Ty - GPĐKKD: 0000000 do sở KH & ĐT TP. HCM cấp ngày 03/03/2030. Địa chỉ: 350-352 Võ Văn Kiệt, Phường Cô Giang, Quận 1, Thành phố Hồ Chí Minh, Việt Nam. Điện thoại: 008.3019.8292.',
                },
            ];
            res.status(200).json(footerItems);
        };
    }
}
const footerController = new FooterController();
exports.default = footerController;
