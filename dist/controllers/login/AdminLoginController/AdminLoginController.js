"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = __importStar(require("jsonwebtoken"));
const ENV_1 = __importDefault(require("../../../core/ENV"));
const BaseAdminLoginController_1 = __importDefault(require("./BaseAdminLoginController"));
class AdminLoginController extends BaseAdminLoginController_1.default {
    constructor() {
        super();
        // [POST] /signIn
        this.signIn = async (req, res) => {
            const { userName, password } = req.body;
            if (!userName || !password) {
                return res.status(200).json({
                    status: 'failure',
                });
            }
            // check if logged
            const requestTokenPayload = req.tokenPayload;
            if (requestTokenPayload && requestTokenPayload.userId.length !== 0) {
                return res.status(200).json({
                    status: 'successfully'
                });
            }
            // check
            const isLoginSuccessfully = await this.check(userName, password);
            // failure
            if (!isLoginSuccessfully) {
                return res.status(200).json({
                    status: 'failure',
                });
            }
            // successfully
            const tokenPayload = {
                userId: (3451245421).toString(16),
            };
            const accessToken = jwt.sign(tokenPayload, ENV_1.default.ACCESS_TOKEN_SECRET);
            return res.status(200).json({
                status: 'successfully',
                accessToken
            });
        };
    }
    static get instance() {
        if (!this._instance)
            this._instance = new AdminLoginController();
        return this._instance;
    }
}
exports.default = AdminLoginController;
