"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const RegExpHub_1 = __importDefault(require("../../../core/RegExpHub"));
class BaseOrderController {
    constructor() {
        this.getTimeQuery = (property) => {
            if (typeof property !== 'string')
                return undefined;
            if (!RegExpHub_1.default.date.test(property))
                return undefined;
            return new Date(property);
        };
        this.makeOrderResponse = async (doc) => {
            if (!doc)
                return undefined;
            return {
                _id: doc._id.toString(),
                customerName: doc.customerName,
                phoneNumber: doc.phoneNumber,
                address: doc.address,
                total: doc.total,
                orderStatus: doc.orderStatus,
                createdAt: doc.createdAt.toJSON(),
                updatedAt: doc.updatedAt.toJSON(),
                orderItems: doc.orderItems.map((orderItem) => {
                    return {
                        productId: orderItem.productId.toString(),
                        productName: orderItem.productName,
                        price: orderItem.price,
                        quantity: orderItem.quantity,
                    };
                })
            };
        };
    }
    makeKeyByDay(d) {
        const dd = d.getDate().toString().padStart(2, '0');
        const mm = d.getMonth().toString().padStart(2, '0');
        const yyyy = d.getFullYear().toString();
        return `${yyyy}-${mm}-${dd}`;
    }
}
exports.default = BaseOrderController;
