"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Order_1 = __importDefault(require("../../../database/models/Order"));
const EOrderStatus_1 = __importDefault(require("../../../database/types/EOrderStatus"));
const calc_1 = __importDefault(require("../../../functions/calc"));
const BaseOrderController_1 = __importDefault(require("./BaseOrderController"));
class OrderController extends BaseOrderController_1.default {
    constructor() {
        super();
        this.baseStatistics = async (req, res) => {
            const orders = await Order_1.default.find({}, {
                total: 1,
                createdAt: 1,
            });
            const maxDate = (new Date()).toJSON();
            let minDate = maxDate;
            orders.forEach(order => {
                if (minDate > order.createdAt.toJSON()) {
                    minDate = order.createdAt.toJSON();
                }
            });
            const totalEarned = orders.reduce((total, order) => {
                total += order.total;
                return total;
            }, 0);
            const totalOrdersPlaced = orders.length;
            const numberOfDates = (() => {
                const _numberOfDates = calc_1.default.LapisDate.millisecondsToNumberOfDates((new Date(maxDate)).getTime() - (new Date(minDate)).getTime());
                // avoid divide by 0
                if (_numberOfDates < 1)
                    return 1;
                return _numberOfDates;
            })();
            const averageEarnedInDays = Math.round((totalEarned / numberOfDates) * 100) / 100;
            const averageOrdersPlacedInDays = Math.round((totalOrdersPlaced / numberOfDates) * 100) / 100;
            return res.status(200).json({
                totalEarned: totalEarned || 0,
                totalOrdersPlaced: totalOrdersPlaced || 0,
                averageEarnedInDays: averageEarnedInDays || 0,
                averageOrdersPlacedInDays: averageOrdersPlacedInDays || 0,
            });
        };
        this.list = async (req, res) => {
            const startAt = this.getTimeQuery(req.query.startAt) || calc_1.default.LapisDate.plus(-30); // 30 day ago
            const endAt = this.getTimeQuery(req.query.endAt) || calc_1.default.LapisDate.plus(1);
            const orders = await Order_1.default.find({
                createdAt: {
                    $gte: startAt,
                    $lte: endAt,
                }
            })
                .sort({ createdAt: -1 });
            const ordersResponse = [];
            for (const order of orders) {
                const orderResponse = await this.makeOrderResponse(order);
                if (!orderResponse)
                    return;
                ordersResponse.push(orderResponse);
            }
            res.status(200).json(ordersResponse);
            res.end();
            return;
        };
        // [PATCH] /update-status/:orderId
        this.updateStatus = async (req, res) => {
            const orderId = req.params.orderId;
            const orderStatus = req.body.orderStatus;
            if (typeof orderStatus !== 'string' || !Object.values(EOrderStatus_1.default).includes(orderStatus)) {
                return res.status(400).send();
            }
            if (!orderId || !mongoose_1.default.Types.ObjectId.isValid(orderId)) {
                return res.status(400).send();
            }
            const order = await Order_1.default.findById(orderId);
            if (!order) {
                return res.status(404).send();
            }
            order.orderStatus = orderStatus;
            try {
                await order.save();
                return res.status(200).json(await this.makeOrderResponse(order));
            }
            catch {
                return res.status(500).send();
            }
        };
        // [GET] /find/:id
        this.find = async (req, res) => {
            const id = req.params.id;
            if (!id || !mongoose_1.default.Types.ObjectId.isValid(id)) {
                return res.status(404).send();
            }
            const orders = await Order_1.default.findById(new mongoose_1.default.Types.ObjectId(id));
            if (!orders) {
                return res.status(404).send();
            }
            res.status(200).json(orders.toJSON());
            return;
        };
    }
    static get instance() {
        if (!this._instance)
            this._instance = new OrderController();
        return this._instance;
    }
}
exports.default = OrderController;
