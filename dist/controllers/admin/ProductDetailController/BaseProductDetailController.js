"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const ProductsCache_1 = __importDefault(require("../../../cache/ProductsCache"));
const ProductDetail_1 = __importDefault(require("../../../database/models/ProductDetail"));
class BaseProductDetailController {
    constructor() {
        this.makeProductDetail = async (formData, _productDetail = null) => {
            const productDetail = _productDetail || new ProductDetail_1.default();
            productDetail._id = new mongoose_1.default.Types.ObjectId(formData._id);
            productDetail.images = formData.images || [];
            productDetail.information = formData.information || '';
            productDetail.promotion = formData.promotion || '';
            productDetail.specifications = formData.specifications || [];
            productDetail.description = formData.description || '';
            return productDetail;
        };
        this.validateFormData = async (formData) => {
            const { _id, images, information, promotion, specifications, description, } = formData;
            // id
            const validateId = async () => {
                if (typeof _id !== 'string')
                    return false;
                if (!mongoose_1.default.Types.ObjectId.isValid(_id))
                    return false;
                if (!(await ProductsCache_1.default.getInstance()).mapId.has(_id))
                    return false;
                return true;
            };
            if (!(await validateId()))
                return {
                    status: 'invalid',
                    message: 'Invalid id'
                };
            // images
            const validateImages = () => {
                if (typeof images === 'undefined')
                    return true;
                if (!Array.isArray(images))
                    return false;
                for (const item in images) {
                    if (typeof item !== 'string')
                        return false;
                }
                return true;
            };
            if (!validateImages())
                return {
                    status: 'invalid',
                    message: 'Invalid images'
                };
            // information
            if (!['undefined', 'string'].includes(typeof information))
                return {
                    status: 'invalid',
                    message: 'Invalid information'
                };
            // promotion
            if (!['undefined', 'string'].includes(typeof promotion))
                return {
                    status: 'invalid',
                    message: 'Invalid promotion'
                };
            // specifications
            const validateSpecifications = () => {
                if (typeof specifications === 'undefined')
                    return true;
                if (!Array.isArray(specifications))
                    return false;
                for (const item of specifications) {
                    if (Object.keys(item).length !== 2)
                        return false;
                    if (typeof item.name !== 'string')
                        return false;
                    if (typeof item.value !== 'string')
                        return false;
                }
                return true;
            };
            if (!validateSpecifications())
                return {
                    status: 'invalid',
                    message: 'Invalid specifications'
                };
            // description
            if (!['undefined', 'string'].includes(typeof description))
                return {
                    status: 'invalid',
                    message: 'Invalid description'
                };
            return {
                status: 'valid',
                message: ''
            };
        };
    }
}
exports.default = BaseProductDetailController;
