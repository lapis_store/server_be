"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const lapisLog_1 = __importDefault(require("../../../core/lapisLog"));
const ProductDetail_1 = __importDefault(require("../../../database/models/ProductDetail"));
const BaseProductDetailController_1 = __importDefault(require("./BaseProductDetailController"));
class ProductDetailController extends BaseProductDetailController_1.default {
    constructor() {
        super();
        // [POST] /create
        this.create = async (req, res) => {
            const formData = req.body;
            if (typeof formData !== 'object') {
                return res.status(400).json({
                    message: 'Invalid request formData',
                });
            }
            // valid
            const validResult = await this.validateFormData(formData);
            if (validResult.status === 'invalid') {
                return res.status(400).json({
                    message: validResult.message,
                });
            }
            // make
            const productDetail = await this.makeProductDetail(formData);
            // save
            try {
                await productDetail.save();
                res.status(201).json(productDetail);
            }
            catch (e) {
                (0, lapisLog_1.default)('ERROR', e.message);
                return res.status(500).send();
            }
        };
        // [PATCH] update
        this.update = async (req, res) => {
            const formData = req.body;
            if (typeof formData !== 'object') {
                return res.status(400).json({
                    message: 'Invalid request formData',
                });
            }
            // valid
            const validResult = await this.validateFormData(formData);
            if (validResult.status === 'invalid') {
                return res.status(400).json({
                    message: validResult.message,
                });
            }
            // make
            const tmpProductDetail = await ProductDetail_1.default.findById(formData._id);
            if (!tmpProductDetail) {
                return res.status(404).send();
            }
            const productDetail = await this.makeProductDetail(formData, tmpProductDetail);
            // save
            try {
                await productDetail.save();
                res.status(200).json(productDetail);
            }
            catch (e) {
                (0, lapisLog_1.default)('ERROR', e.message);
                return res.status(500).send();
            }
        };
        // [GET] /find/:productId
        this.find = async (req, res) => {
            const productId = req.params.id;
            if (typeof productId !== 'string' || !mongoose_1.default.Types.ObjectId.isValid(productId)) {
                return res.status(404).send();
            }
            const productDetail = await ProductDetail_1.default.findById(productId);
            // not found
            if (!productDetail) {
                return res.status(404).send();
            }
            res.status(200).json(productDetail);
        };
    }
    static get instance() {
        if (!this._instance)
            this._instance = new ProductDetailController();
        return this._instance;
    }
}
exports.default = ProductDetailController;
