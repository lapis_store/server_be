"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const CategoriesCache_1 = __importDefault(require("../../../cache/CategoriesCache"));
const ProductsCache_1 = __importDefault(require("../../../cache/ProductsCache"));
const Product_1 = __importDefault(require("../../../database/models/Product"));
const BaseProductController_1 = __importDefault(require("./BaseProductController"));
class ProductController extends BaseProductController_1.default {
    constructor() {
        super();
        // [POST] /product/create
        this.create = async (req, res) => {
            // get and check request body
            const formData = req.body;
            if (!formData) {
                return res.status(400).json({
                    message: 'FormData is undefined',
                });
            }
            const validResult = await this.validateFormData(formData);
            if (validResult.status === 'invalid') {
                return res.status(400).json({
                    message: validResult.message,
                });
            }
            const product = await this.makeProduct(formData);
            if (!product) {
                return res.status(500).send(`Can't make product from formData`);
            }
            try {
                await product.save();
                ProductsCache_1.default.isLatest = false;
                return res.status(201).json(product.toObject());
            }
            catch {
                return res.status(500).send('Save product failure');
            }
        };
        // [PATCH] /product/update/:id
        this.update = async (req, res) => {
            const productId = req.params.id;
            if (typeof productId !== 'string' || !mongoose_1.default.Types.ObjectId.isValid(productId)) {
                return res.status(404).send();
            }
            // get and check request body
            const formData = req.body;
            if (!formData) {
                return res.status(400).json({
                    message: 'FormData is undefined',
                });
            }
            const validResult = await this.validateFormData(formData);
            if (validResult.status === 'invalid') {
                return res.status(400).json({
                    message: validResult.message,
                });
            }
            const tmpProduct = await Product_1.default.findById(productId);
            if (!tmpProduct) {
                return res.status(404).send();
            }
            const product = await this.makeProduct(formData, tmpProduct);
            if (!product) {
                return res.status(500).send(`Can't make product from formData`);
            }
            try {
                await product.save();
                ProductsCache_1.default.isLatest = false;
                return res.status(200).json(product.toObject());
            }
            catch {
                return res.status(500).send('Save product failure');
            }
        };
        // [DELETE] /product/remove/:id
        this.remove = async (req, res) => {
            const productId = req.params.id;
            if (typeof productId !== 'string' || !mongoose_1.default.Types.ObjectId.isValid(productId)) {
                return res.status(404).send();
            }
            try {
                const product = await Product_1.default.findByIdAndDelete(productId);
                if (product) {
                    ProductsCache_1.default.isLatest = false;
                    return res.status(200).json({
                        _id: String(product._id.toString()),
                    });
                }
                return res.status(404).send();
            }
            catch {
                return res.status(500).send();
            }
        };
        // [GET] /product/list
        this.list = async (req, res) => {
            const categoryId = req.query.categoryId;
            const sortBy = -1;
            let products = (await ProductsCache_1.default.getInstance()).listProducts.map(item => item.data);
            if (typeof categoryId === 'string' && mongoose_1.default.Types.ObjectId.isValid(categoryId)) {
                const categories = (await CategoriesCache_1.default.getInstance()).getAllChildren(categoryId);
                categories.add(categoryId);
                if (categories.size !== 0) {
                    products = products.filter((product) => {
                        if (!product.categoryId)
                            return false;
                        return categories.has(product.categoryId.toString());
                    });
                }
            }
            products.sort((a, b) => {
                const aCreatedAt = a.createdAt;
                const bCreatedAt = b.createdAt;
                if (aCreatedAt > bCreatedAt)
                    return 1 * sortBy;
                if (aCreatedAt < bCreatedAt)
                    return -1 * sortBy;
                return 0;
            });
            return res.status(200).json(products);
        };
        // [GET] /product/find/:id
        this.find = async (req, res) => {
            const productId = req.params.id;
            if (typeof productId !== 'string' || !mongoose_1.default.Types.ObjectId.isValid(productId)) {
                return res.status(404).send();
            }
            const product = (await ProductsCache_1.default.getInstance()).mapId.get(productId);
            if (!product) {
                return res.status(404).send();
            }
            return res.status(200).json(product.data);
        };
        // [GET] /product/image-name/:id
        this.imageName = async (req, res) => {
            const productId = req.params.id;
            if (typeof productId !== 'string' || !mongoose_1.default.Types.ObjectId.isValid(productId)) {
                return res.status(404).send();
            }
            const product = (await ProductsCache_1.default.getInstance()).mapId.get(productId);
            if (!product) {
                return res.status(404).send();
            }
            return res.status(200).send(product.data.image);
        };
        // [GET] /product/check-slug
        this.checkSlug = async (req, res) => {
            const slug = req.query.slug && String(req.query.slug);
            const productId = req.query.id && String(req.query.id);
            if (!['string', 'undefined'].includes(typeof productId)) {
                return res.status(400).send();
            }
            if (typeof slug !== 'string' || slug.length === 0) {
                return res.status(400).send();
            }
            const product = (await ProductsCache_1.default.getInstance()).mapSlug.get(slug);
            if (!product) {
                return res.status(200).json({
                    isExisted: false,
                });
            }
            if (productId === product.id.toString()) {
                return res.status(200).json({
                    isExisted: false,
                });
            }
            // response
            return res.status(200).json({
                isExisted: true,
            });
        };
    }
    static get instance() {
        if (!this._instance)
            this._instance = new ProductController();
        return this._instance;
    }
}
exports.default = ProductController;
