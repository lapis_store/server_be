"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const ENV_1 = __importDefault(require("../../../core/ENV"));
const lapisLog_1 = __importDefault(require("../../../core/lapisLog"));
class BaseHomeController {
    constructor() {
        this.fileName = 'home.json';
        this.save = async (formData) => {
            return new Promise((resolve, reject) => {
                const filePath = path_1.default.join(ENV_1.default.DATA_DIR, this.fileName);
                try {
                    const data = JSON.stringify(formData);
                    fs_1.default.writeFileSync(filePath, data);
                    resolve(true);
                }
                catch {
                    (0, lapisLog_1.default)('ERROR', `[BaseHomeController] Can not save file to "${filePath}"`);
                    resolve(false);
                }
            });
        };
        this.validateBanner = (banner) => {
            if (Object.keys(banner).length !== 3)
                return false;
            const { alt, image, url, } = banner;
            if (typeof alt !== 'string')
                return false;
            if (typeof image !== 'string')
                return false;
            if (typeof url !== 'string')
                return false;
            return true;
        };
        this.validateBanners = (banners) => {
            for (const banner of banners) {
                if (!this.validateBanner(banner))
                    return false;
            }
            return true;
        };
        this.validateFormData = async (formData) => {
            if (Object.keys(formData).length !== 4)
                return {
                    status: 'invalid',
                    message: 'Invalid formData'
                };
            const { carousel, bannersRightCarousel, bannersBelowCarousel, brand } = formData;
            if (!this.validateBanners(carousel))
                return {
                    status: 'invalid',
                    message: 'Invalid carousel'
                };
            if (!this.validateBanners(bannersRightCarousel))
                return {
                    status: 'invalid',
                    message: 'Invalid bannersRightCarousel'
                };
            if (!this.validateBanners(bannersBelowCarousel))
                return {
                    status: 'invalid',
                    message: 'Invalid bannersBelowCarousel'
                };
            if (!this.validateBanners(brand))
                return {
                    status: 'invalid',
                    message: 'Invalid brand'
                };
            return {
                status: 'valid',
                message: ''
            };
        };
    }
}
exports.default = BaseHomeController;
