"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lapisLog_1 = __importDefault(require("../../../core/lapisLog"));
const BaseHomeController_1 = __importDefault(require("./BaseHomeController"));
class HomeController extends BaseHomeController_1.default {
    constructor() {
        super();
        // [POST] /create
        this.create = async (req, res) => {
            const formData = req.body;
            if (!formData) {
                return res.status(400).send();
            }
            const validResult = await this.validateFormData(formData);
            if (validResult.status === 'invalid') {
                return res.status(400).json({
                    message: validResult.message
                });
            }
            try {
                const saveResult = await this.save(formData);
                if (!saveResult) {
                    return res.status(500).send('save file failure');
                }
                return res.status(201).json(formData);
            }
            catch (e) {
                (0, lapisLog_1.default)('ERROR', e.message);
                return res.status(500).send();
            }
        };
    }
    static get instance() {
        if (!this._instance)
            this._instance = new HomeController();
        return this._instance;
    }
}
exports.default = HomeController;
