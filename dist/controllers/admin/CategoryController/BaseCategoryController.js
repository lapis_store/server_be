"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const Category_1 = __importDefault(require("../../../database/models/Category"));
class BaseCategoryController {
    constructor() {
        this.validFormData = async (formData) => {
            const { title, image, parentId, index, } = formData;
            if (typeof index !== 'number' || !isFinite(index)) {
                return {
                    status: 'invalid',
                    message: 'Invalid index'
                };
            }
            // title
            if (typeof title !== 'string') {
                return {
                    status: 'invalid',
                    message: 'Invalid title'
                };
            }
            // image
            if (!['undefined', 'string'].includes(typeof image)) {
                return {
                    status: 'invalid',
                    message: 'Invalid image'
                };
            }
            // parentId
            if (!['undefined', 'string'].includes(typeof parentId)) {
                return {
                    status: 'invalid',
                    message: 'Invalid parentId'
                };
            }
            if (typeof parentId === 'string' && parentId !== 'undefined') {
                if (!mongoose_1.default.Types.ObjectId.isValid(parentId))
                    return {
                        status: 'invalid',
                        message: 'Invalid parentId'
                    };
                const isExists = await Category_1.default.exists({ _id: parentId });
                if (!isExists)
                    return {
                        status: 'invalid',
                        message: 'parentId not exists'
                    };
            }
            return {
                status: 'valid',
                message: ''
            };
        };
        this.makeCategory = async (formData, _category) => {
            const category = _category || new Category_1.default();
            category.title = formData.title;
            category.index = formData.index;
            category.image = formData.image;
            if (typeof formData.parentId === 'undefined' || formData.parentId === 'undefined') {
                category.parentId = undefined;
            }
            else {
                category.parentId = new mongoose_1.default.Types.ObjectId(formData.parentId);
            }
            return category;
        };
    }
}
exports.default = BaseCategoryController;
