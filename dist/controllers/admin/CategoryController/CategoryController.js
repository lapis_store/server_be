"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const CategoriesCache_1 = __importDefault(require("../../../cache/CategoriesCache"));
const lapisLog_1 = __importDefault(require("../../../core/lapisLog"));
const Category_1 = __importDefault(require("../../../database/models/Category"));
const BaseCategoryController_1 = __importDefault(require("./BaseCategoryController"));
class CategoryController extends BaseCategoryController_1.default {
    constructor() {
        super();
        // [POST] /create
        this.create = async (req, res) => {
            const formData = req.body;
            if (!formData) {
                return res.status(400).send();
            }
            // validation
            const validResult = await this.validFormData(formData);
            if (validResult.status === 'invalid') {
                return res.status(400).json({
                    message: validResult.message
                });
            }
            // create new category
            const category = await this.makeCategory(formData);
            // save
            try {
                await category.save();
                CategoriesCache_1.default.isLatest = false;
                return res.status(201).json(category);
            }
            catch (e) {
                (0, lapisLog_1.default)('ERROR', `Can't save category: ${e.message}`);
                return res.status(500).send();
            }
        };
        // [PATCH] /update/:id
        this.update = async (req, res, next) => {
            const categoryId = req.params.id;
            // Get category from client
            const formData = req.body;
            if (!formData) {
                return res.status(400).send();
            }
            const validResult = await this.validFormData(formData);
            if (validResult.status === 'invalid') {
                return res.status(400).json({
                    message: validResult.message
                });
            }
            // Find category and check
            let category = await Category_1.default.findById(categoryId);
            if (!category || category === null) {
                return res.status(404).send();
            }
            category = await this.makeCategory(formData, category);
            // Check if fullPath of category is endless "circular loops"
            if (await category.fullPathIsCircularLoops()) {
                return res.status(400).json({
                    message: `Invalid parentId !. parentId can't contain child id`,
                });
            }
            // Save
            try {
                await category.save();
                CategoriesCache_1.default.isLatest = false;
                return res.status(200).json(category);
            }
            catch (e) {
                (0, lapisLog_1.default)('ERROR', `Can't save category: ${e.message}`);
                return res.status(400).send();
            }
        };
        // [DELETE] /category/remove/:id
        this.remove = async (req, res, next) => {
            const categoryId = req.params.id;
            try {
                // find
                const category = await Category_1.default.findByIdAndRemove(categoryId);
                // response
                if (!category || category === null) {
                    return res.status(404).send();
                }
                const children = await Category_1.default.find({ parentId: category.id });
                for (const child of children) {
                    child.parentId = undefined;
                    await child.save();
                }
                CategoriesCache_1.default.isLatest = false;
                return res.status(200).json(category.toObject());
            }
            catch (e) {
                (0, lapisLog_1.default)('ERROR', e.message);
            }
            return res.status(404).send();
        };
        // [DELETE] /category/removeAll/:id
        this.removeAll = async (req, res, next) => {
            const categoryId = req.params.id;
            try {
                // find
                const category = await Category_1.default.findByIdAndRemove(categoryId);
                // response
                if (!category || category === null) {
                    return res.status(404).send();
                }
                const removeAllChildren = async (parentId) => {
                    const children = await Category_1.default.find({ parentId: parentId });
                    if (children.length === 0)
                        return;
                    for (const child of children) {
                        await removeAllChildren(child._id);
                        child.remove();
                    }
                };
                await removeAllChildren(category.id);
                CategoriesCache_1.default.isLatest = false;
                return res.status(200).json(category.toObject());
            }
            catch (e) {
                (0, lapisLog_1.default)('ERROR', e.message);
            }
            return res.status(404).send();
        };
        // [GET] /category/find/:id
        this.find = async (req, res, next) => {
            const categoryId = req.params.id;
            // find
            const category = await Category_1.default.findById(categoryId);
            // response
            if (!category) {
                return res.status(404).send();
            }
            return res.status(200).json({
                ...category.toObject(),
                fullPath: await category.fullPath,
            });
        };
        // [GET] /list
        this.list = async (req, res) => {
            const categories = (await CategoriesCache_1.default.getInstance()).data;
            return res.status(200).json(categories);
        };
    }
    static get instance() {
        if (!this._instance)
            this._instance = new CategoryController;
        return this._instance;
    }
}
exports.default = CategoryController;
