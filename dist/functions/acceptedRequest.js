"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const acceptedRequest = async (req, res) => {
    res.status(200).send();
    // res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization, auth");
    res.end();
};
exports.default = acceptedRequest;
