"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const LapisDate_1 = __importDefault(require("./LapisDate"));
const calc = {
    LapisDate: LapisDate_1.default,
};
exports.default = calc;
