"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class LapisDate {
    static dateToMilliseconds(d) {
        // one date = 24*3600*1000 = 86400000 milliseconds
        return d * 86400000;
    }
    static millisecondsToNumberOfDates(milliseconds) {
        return milliseconds / 86400000;
    }
    static plus(numberOfDate, d = new Date()) {
        const dTime = d.getTime();
        const plusTime = this.dateToMilliseconds(numberOfDate);
        return new Date(dTime + plusTime);
    }
}
exports.default = LapisDate;
