"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const removeVietnameseAccents_1 = __importDefault(require("./removeVietnameseAccents"));
const standardizationKeyword = (keyword) => {
    let result = keyword.replace(/[\s]+/g, ' ').trim().toLowerCase();
    return (0, removeVietnameseAccents_1.default)(result);
};
exports.default = standardizationKeyword;
