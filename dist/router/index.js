"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const body_parser_1 = __importDefault(require("body-parser"));
const express_1 = __importDefault(require("express"));
const adminRouter_1 = __importDefault(require("./adminRouter"));
const accessControlAllow_1 = __importDefault(require("../middleware/accessControlAllow"));
const path_1 = __importDefault(require("path"));
const userRouter_1 = __importDefault(require("./userRouter"));
const loginRouter_1 = __importDefault(require("./loginRouter"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const authenticateToken_1 = __importDefault(require("../middleware/authenticateToken"));
const ENV_1 = __importDefault(require("../core/ENV"));
function router(app) {
    if (ENV_1.default.IS_DEV) {
        // app.use(morgan('combined'));
    }
    // access control
    app.use(accessControlAllow_1.default);
    // static file
    app.use('/static', express_1.default.static(path_1.default.join(__dirname, '../../public')));
    //
    app.use((0, cookie_parser_1.default)());
    //
    app.use(body_parser_1.default.urlencoded({ extended: false }));
    app.use(body_parser_1.default.json());
    //
    app.use('/admin', authenticateToken_1.default, adminRouter_1.default);
    app.use('/user', userRouter_1.default);
    app.use('/login', loginRouter_1.default);
}
exports.default = router;
