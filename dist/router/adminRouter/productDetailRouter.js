"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ProductDetailController_1 = __importDefault(require("../../controllers/admin/ProductDetailController"));
const productDetailController = ProductDetailController_1.default.instance;
const productDetailRouter = express_1.default.Router();
productDetailRouter.post('/create', productDetailController.create);
productDetailRouter.patch('/update', productDetailController.update);
productDetailRouter.get('/find/:id', productDetailController.find);
exports.default = productDetailRouter;
