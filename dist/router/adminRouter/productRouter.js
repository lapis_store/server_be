"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ProductController_1 = __importDefault(require("../../controllers/admin/ProductController"));
const productController = ProductController_1.default.instance;
const productRouter = express_1.default.Router();
productRouter.post('/create', productController.create);
productRouter.patch('/update/:id', productController.update);
productRouter.delete('/remove/:id', productController.remove);
productRouter.get('/find/:id', productController.find);
productRouter.get('/list', productController.list);
productRouter.get('/check-slug', productController.checkSlug);
productRouter.get('/image-name/:id', productController.imageName);
exports.default = productRouter;
