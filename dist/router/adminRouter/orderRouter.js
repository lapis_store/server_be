"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const OrderController_1 = __importDefault(require("../../controllers/admin/OrderController"));
// import acceptedRequest from '../../functions/acceptedRequest';
const orderController = OrderController_1.default.instance;
const orderRouter = express_1.default.Router();
orderRouter.get('/find/:id', orderController.find);
orderRouter.get('/list', orderController.list);
orderRouter.get('/base-statistics', orderController.baseStatistics);
orderRouter.patch('/update-status/:orderId', orderController.updateStatus);
exports.default = orderRouter;
