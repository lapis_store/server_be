"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const productDetailRouter_1 = __importDefault(require("./productDetailRouter"));
const categoryRouter_1 = __importDefault(require("./categoryRouter"));
const productRouter_1 = __importDefault(require("./productRouter"));
const orderRouter_1 = __importDefault(require("./orderRouter"));
const homeRouter_1 = __importDefault(require("./homeRouter"));
const adminRouter = express_1.default.Router();
adminRouter.use('/product', productRouter_1.default);
adminRouter.use('/category', categoryRouter_1.default);
adminRouter.use('/product-detail', productDetailRouter_1.default);
adminRouter.use('/order', orderRouter_1.default);
adminRouter.use('/home', homeRouter_1.default);
exports.default = adminRouter;
