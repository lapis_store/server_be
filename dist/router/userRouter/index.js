"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const categoryRouter_1 = __importDefault(require("./categoryRouter"));
const footerRouter_1 = __importDefault(require("./footerRouter"));
const orderRouter_1 = __importDefault(require("./orderRouter"));
const productRouter_1 = __importDefault(require("./productRouter"));
const searchRouter_1 = __importDefault(require("./searchRouter"));
const userRouter = express_1.default.Router();
userRouter.use('/footer', footerRouter_1.default);
userRouter.use('/categories', categoryRouter_1.default);
userRouter.use('/product', productRouter_1.default);
userRouter.use('/search', searchRouter_1.default);
userRouter.use('/order', orderRouter_1.default);
exports.default = userRouter;
