"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const OrderController_1 = __importDefault(require("../../controllers/user/OrderController"));
const acceptedRequest_1 = __importDefault(require("../../functions/acceptedRequest"));
const orderController = OrderController_1.default.instance;
const orderRouter = express_1.default.Router();
orderRouter.head('/add', acceptedRequest_1.default);
orderRouter.options('/add', acceptedRequest_1.default);
orderRouter.post('/add', orderController.add);
exports.default = orderRouter;
