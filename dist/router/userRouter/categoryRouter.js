"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const CategoryController_1 = __importDefault(require("../../controllers/user/CategoryController/CategoryController"));
const categoryController = CategoryController_1.default.instance;
const categoryRouter = express_1.default.Router();
categoryRouter.get('/list', categoryController.list);
categoryRouter.get('/find/:id', categoryController.find);
exports.default = categoryRouter;
