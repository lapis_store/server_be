"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const footerController_1 = __importDefault(require("../../controllers/user/footerController"));
const footerRouter = express_1.default.Router();
footerRouter.get('/list', footerController_1.default.list);
exports.default = footerRouter;
