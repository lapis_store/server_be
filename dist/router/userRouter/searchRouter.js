"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const SearchController_1 = __importDefault(require("../../controllers/user/SearchController/SearchController"));
const searchController = SearchController_1.default.instance;
const searchRouter = express_1.default.Router();
searchRouter.get('/', searchController.index);
searchRouter.get('/suggestion', searchController.suggestions);
exports.default = searchRouter;
