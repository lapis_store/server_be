"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ProductController_1 = __importDefault(require("../../controllers/user/ProductController/ProductController"));
const productController = ProductController_1.default.instance;
const productRouter = express_1.default.Router();
productRouter.get('/latest-products', productController.latestProducts);
productRouter.get('/list', productController.list);
productRouter.get('/get-by-id/:id', productController.getById);
productRouter.get('/get-by-list-id', productController.getByListId);
productRouter.get('/get-by-slug', productController.getBySlug);
exports.default = productRouter;
