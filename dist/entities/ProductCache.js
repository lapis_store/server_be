"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lapisLog_1 = __importDefault(require("../core/lapisLog"));
class ProductCache {
    constructor(data) {
        if (!data) {
            (0, lapisLog_1.default)('ERROR', `data for ProductCache is null or undefined`);
            throw new Error('data for ProductCache is null or undefined');
        }
        this._data = data.toObject();
    }
    get isPromotion() {
        if (!this._data.isPromotionalPrice)
            return false;
        const currentTime = new Date();
        if (!this._data.promotionStartAt) {
            if (!this._data.promotionEndAt)
                return true;
            if (currentTime < this._data.promotionEndAt)
                return true;
            return false;
        }
        if (!this._data.promotionEndAt) {
            if (!this._data.promotionStartAt)
                return true;
            if (currentTime > this._data.promotionStartAt)
                return true;
            return false;
        }
        if (this._data.promotionStartAt < currentTime && currentTime < this._data.promotionEndAt) {
            return true;
        }
        return false;
    }
    get currentPrice() {
        if (this.isPromotion
            && this._data.promotionPrice !== undefined)
            return this._data.promotionPrice;
        return this._data.price;
    }
    get id() {
        return this._data._id;
    }
    get data() {
        return this._data;
    }
    toResObject() {
        const isPromotion = this.isPromotion;
        return {
            _id: this._data._id.toString(),
            slug: this._data.slug,
            image: this._data.image,
            title: this._data.title,
            isPromotion,
            price: this._data.price,
            promotionPrice: isPromotion ? this._data.promotionPrice : undefined,
            summary: this._data.summary,
        };
    }
}
exports.default = ProductCache;
