"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lapisLog_1 = __importDefault(require("../core/lapisLog"));
const RegExpHub_1 = __importDefault(require("../core/RegExpHub"));
const allowOrigins_1 = __importDefault(require("../config/allowOrigins"));
const originPattern = /^(https?):\/\/([a-z0-9\.\-]+):?(\d+)?$/;
const getPort = (_port = undefined) => {
    if (!_port)
        return 80;
    if (!RegExpHub_1.default.integer.test(_port))
        return -1;
    const port = parseInt(_port);
    if (!isFinite(port))
        return -1;
    return port;
};
const isAllow = (origin) => {
    if (origin === undefined)
        return true; // same origin
    const matchResult = origin.match(originPattern);
    if (!matchResult)
        return false; // origin error
    const [_origin, _protocol, _domain, _port, // type = string|undefined
    ] = matchResult;
    const port = getPort(_port);
    if (port < 0)
        return false;
    // domain
    const allowOriginItem = allowOrigins_1.default.find(item => item.domain === _domain);
    if (!allowOriginItem)
        return false;
    // protocol
    if (!allowOriginItem.protocol.includes(_protocol))
        return false;
    // port
    if (!allowOriginItem.ports.includes(port))
        return false;
    return true;
};
const accessControlAllow = async (req, res, next) => {
    const origin = req.headers.origin;
    if (!isAllow(origin)) {
        (0, lapisLog_1.default)('WARNING', `"${origin}" have been denied`);
        return res.status(403).send();
    }
    res.setHeader('Access-Control-Allow-Origin', origin || '');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', [
        'Origin',
        'X-Requested-With',
        'Content-Type',
        'auth',
        'authorization'
    ]
        .map(item => item.toLowerCase())
        .join(', '));
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    if (req.method.toLocaleUpperCase() === 'OPTIONS') {
        return res.status(202).send();
    }
    next();
    //  // Website you wish to allow to connect
    //  res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:3000');
    //  // Request methods you wish to allow
    //  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    //  // Request headers you wish to allow
    //  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    //  // Set to true if you need the website to include cookies in the requests sent
    //  // to the API (e.g. in case you use sessions)
    //  res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
};
exports.default = accessControlAllow;
