FROM node

WORKDIR /usr/src/app
EXPOSE 3002



COPY ./yarn.lock .
COPY ./package.json .

COPY ./.env.development .
COPY ./.env.production .
COPY ./.env .

COPY ./public ./public
COPY ./dist ./dist


RUN yarn install


ENV NODE_ENV=production

CMD [ "yarn", "start" ]
