import mongoose from 'mongoose';
import Category from '../database/models/Category';
import { ICategoryDocument } from '../database/schemas/CategorySchema';

class CategoriesCache {
    private static _instance?: CategoriesCache;
    private _data: mongoose.LeanDocument<
        mongoose.Document<unknown, any, ICategoryDocument> &
            ICategoryDocument & {
                _id: string | mongoose.Types.ObjectId;
            }
    >[] = [];
    private static _isLatest: boolean = false;

    public static get isLatest() {
        return this._isLatest;
    }

    public static set isLatest(v: boolean) {
        this._isLatest = v;
    }

    public get data() {
        return this._data;
    }

    protected constructor() {}

    public static getInstance = async () => {
        if (!this._instance) this._instance = new CategoriesCache();

        if (!this._isLatest) {
            await this._instance.update();
        }

        return this._instance;
    };

    public findOne = (id: string) => {
        return this._data.find((item) => item._id.toString() === id);
    };

    public getAllChildren = (id: string): Set<string> => {
        const getAllChild = (parentId: string, added: Set<string> = new Set()) => {
            this._data.forEach((item) => {
                if (item.parentId?.toString() !== parentId) return;
                if (added.has(item._id.toString())) return;

                added.add(item._id.toString());
                getAllChild(item._id.toString(), added);
            });

            return added;
        };

        return getAllChild(id);
    };

    private update = async () => {
        const categories = await Category.find();

        this._data = categories.map((item) => {
            return item.toObject();
        });

        CategoriesCache.isLatest = true;
    };
}

export default CategoriesCache;
