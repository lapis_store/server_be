import lapisLog from '../core/lapisLog';
import Product from '../database/models/Product';
import ProductCache from '../entities/ProductCache';
import removeVietnameseAccents from '../functions/removeVietnameseAccents';
import standardizationKeyword from '../functions/standardizationKeyword';

class ProductsCache {
    private static _instance: ProductsCache | undefined = undefined;
    private _mapId: Map<string, ProductCache> = new Map();
    private _mapSlug: Map<string, ProductCache> = new Map();
    private _listProducts: ProductCache[] = [];
    private _suggestions: {
        key: string;
        product: ProductCache;
    }[] = [];

    private static _isLatest: boolean = false;

    private constructor() {}

    public get mapId() {
        return this._mapId;
    }

    public get mapSlug() {
        return this._mapSlug;
    }

    public get listProducts() {
        return this._listProducts;
    }

    public get suggestions() {
        return this._suggestions;
    }

    public static get isLatest() {
        return this._isLatest;
    }

    public static set isLatest(v: boolean) {
        this._isLatest = v;
    }

    public static async getInstance(): Promise<ProductsCache> {
        if (!this._instance) {
            this._instance = new ProductsCache();
        }

        if (!this.isLatest) {
            await this._instance.update();
        }

        return this._instance;
    }

    public async update() {
        const productsDocs = await Product.find();

        this._mapId = new Map();
        this._mapSlug = new Map();
        this._suggestions = [];
        this._listProducts = [];

        for (const item of productsDocs) {
            if (!item) {
                lapisLog('ERROR', 'ProductsCache error with item null || undefined');
                continue;
            }

            const _product = new ProductCache(item);

            this._mapId.set(_product.id.toString(), _product);
            this._mapSlug.set(_product.data.slug, _product);
            this._suggestions.push({
                key: standardizationKeyword(_product.data.title),
                product: _product,
            });
            this._listProducts.push(_product);
        }

        ProductsCache.isLatest = true;
    }
}

export default ProductsCache;
