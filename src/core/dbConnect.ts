import mongoose from 'mongoose';
import lapisLog from './lapisLog';
import ENV from './ENV';

const makeConnectionString = () => {
    const params: {
        [key: string]: string;
    } = {
        directConnection: String(ENV.DIRECT_CONNECTION),
        authMechanism: 'DEFAULT',
        authSource: ENV.AUTH_SOURCE,
    };

    const strParams = Object.keys(params).map((key) => {
        return [key, params[key]].join('=');
    });

    // mongodb://username:password@db.lapisblog.com:27017/?directConnection=false&authMechanism=DEFAULT&authSource=check
    return `mongodb://${ENV.DATABASE_USERNAME}:${ENV.DATABASE_PASSWORD}@${ENV.DATABASE_HOST}/${
        ENV.DATABASE_NAME
    }?${strParams.join('&')}`;
};

export default async function (): Promise<boolean> {
    lapisLog('WAITTING', `Connecting to ${ENV.DATABASE_NAME}`);
    const connectionString = makeConnectionString();
    try {
        await mongoose.connect(connectionString, {
            keepAlive: true,
        });
        lapisLog('SUCCESS', `Connect to ${ENV.DATABASE_NAME} successfully !`);
        return true;
    } catch (e) {
        lapisLog('ERROR', `Connect to ${ENV.DATABASE_NAME} failed !`);
    }
    return false;
}
