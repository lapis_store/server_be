interface IValidateResult {
    status: 'valid' | 'invalid';
    message: string;
}

export default IValidateResult;
