import { Request, Response, NextFunction } from 'express';
import mongoose from 'mongoose';

// Middleware
export type TLapisMiddleware = (
    req: Request,
    res: Response,
    next: NextFunction,
) => any | void | Promise<void> | undefined;

export type TLapisMongooseDocument<T> =
    | (mongoose.Document<unknown, any, T> &
          T & {
              _id: string | mongoose.Types.ObjectId;
          })
    | null;

// Mongoose document
// export type TLapisMongooseDocument<T> = (mongoose.Document<unknown, any, T> &
//     T & {
//         _id: mongoose.Types.ObjectId;
//     })|null;

// General Schema
export interface ILapisGeneralSchema {
    _id: mongoose.Types.ObjectId | string;
    createdAt: Date;
    updatedAt: Date;
}

export interface ILapisMakeResult {
    status: 'successfully' | 'failed';
    message: string;
}
