import path from 'path';

class ENV {
    public static readonly DATA_DIR: string = path.join(__dirname, '../../public/data');

    public static readonly SHOP_NAME: string = process.env.SHOP_NAME || '';

    public static readonly IS_DEV: boolean = (process.env.NODE_ENV && process.env.NODE_ENV === 'development') || false;

    public static readonly PORT: number = process.env.PORT ? parseInt(process.env.PORT) : 8080;

    // DATABASE
    public static readonly DATABASE_HOST: string = process.env.DATABASE_HOST || '127.0.0.1:27017';

    public static readonly DATABASE_NAME: string = process.env.DATABASE_NAME || 'default';
    public static readonly DATABASE_USERNAME: string = process.env.DATABASE_USERNAME || '';
    public static readonly DATABASE_PASSWORD: string = process.env.DATABASE_PASSWORD || '';
    public static readonly DIRECT_CONNECTION: boolean = String(process.env.DIRECT_CONNECTION) === 'true' || false;
    public static readonly AUTH_SOURCE: string = process.env.AUTH_SOURCE || '';

    // TOKEN
    public static readonly ACCESS_TOKEN_SECRET: string = process.env.ACCESS_TOKEN_SECRET || 'default';

    public static readonly REFRESH_TOKEN_SECRET: string = process.env.REFRESH_TOKEN_SECRET || 'default';

    // TIME EXPIRE TOKEN
    public static readonly ACCESS_TOKEN_EXPIRE_IN: string = process.env.ACCESS_TOKEN_EXPIRE_IN || '15m';

    public static readonly REFRESH_TOKEN_EXPIRE_IN: string = process.env.REFRESH_TOKEN_EXPIRE_IN || '15m';
}

export default ENV;
