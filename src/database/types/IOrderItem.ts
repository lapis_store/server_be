import mongoose from 'mongoose';

interface IOrderItem {
    productId: mongoose.Types.ObjectId;
    productName: string;
    price: number;
    quantity: number;
}

export default IOrderItem;
