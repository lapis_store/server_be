import mongoose from 'mongoose';
import { ILapisGeneralSchema, TLapisMongooseDocument } from '../../core/LapisTypes';
import EOrderStatus from '../types/EOrderStatus';
import IOrderItem from '../types/IOrderItem';

export interface IOrderBaseSchema extends ILapisGeneralSchema {
    userId?: mongoose.Types.ObjectId;
    customerName: string;
    orderStatus: EOrderStatus;
    phoneNumber: string;
    email: string;
    address: string;
    total: number;
    orderItems: IOrderItem[];
}

export interface IOrderDocument extends IOrderBaseSchema {}

export interface IOrderModel extends mongoose.Model<IOrderDocument> {}

export type TOrderDocument = TLapisMongooseDocument<IOrderDocument>;

const OrderSchema = new mongoose.Schema<IOrderDocument, IOrderModel>({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        default: undefined,
    },

    customerName: {
        type: String,
        required: true,
    },

    orderStatus: {
        type: String,
        default: EOrderStatus.unconfirmed,
    },

    phoneNumber: {
        type: String,
        validate: {
            validator: (v: string) => {
                const regexPhoneNumber = /^[0-9]{10}$/;
                return regexPhoneNumber.test(v);
            },
            message: () => {
                return `Invalid phone number`;
            },
        },
        required: true,
    },

    email: {
        type: String,
        default: undefined,
    },

    address: {
        type: String,
        required: true,
    },

    total: {
        type: Number,
        required: true,
    },

    orderItems: [
        {
            productId: {
                type: mongoose.Schema.Types.ObjectId,
                required: true,
            },

            productName: {
                type: String,
                required: true,
            },

            price: {
                type: Number,
                validate: {
                    validator: (v: number) => {
                        return v > 0;
                    },
                    message: (e) => {
                        return `Price can't be less than 0`;
                    },
                },
                required: true,
            },

            quantity: {
                type: Number,
                validate: {
                    validator: (v: number) => {
                        return v > 0;
                    },
                    message: (e) => {
                        return `Quantity can't be less than 0`;
                    },
                },
                required: true,
            },
        },
    ],

    updatedAt: {
        type: Date,
        default: () => new Date(),
    },

    createdAt: {
        type: Date,
        immutable: true,
        default: () => new Date(),
    },
});

export default OrderSchema;
