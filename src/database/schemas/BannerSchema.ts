import mongoose from 'mongoose';
import { ILapisGeneralSchema } from '../../core/LapisTypes';

export interface IBannerSchema {
    image: string;
    url: string;
    alt: string;
}

const BannerSchema = new mongoose.Schema<IBannerSchema>({
    image: {
        type: String,
        default: '#',
    },
    url: {
        type: String,
        default: '#',
    },
    alt: {
        type: String,
        default: '',
    },
});

export default BannerSchema;
