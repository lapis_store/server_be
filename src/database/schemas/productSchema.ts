import mongoose from 'mongoose';

import { TLapisMongooseDocument, ILapisGeneralSchema } from '../../core/LapisTypes';
import Category from '../models/Category';
import Product from '../models/Product';

interface IProductSchema extends ILapisGeneralSchema {
    slug: string;
    categoryId?: mongoose.Types.ObjectId;
    keyword: string;
    title: string;
    price: number;
    isPromotionalPrice: boolean;
    promotionPrice: number;
    promotionStartAt?: Date;
    promotionEndAt?: Date;
    summary: string;
    image: string;
}

interface IProductDocument extends IProductSchema {
    currentPrice: Number;
    isPromotion: boolean;
    slugIsExisted: () => Promise<boolean | undefined>;
}

// static methods
interface IProductModel extends mongoose.Model<IProductDocument> {
    findBySlug: (slug: string) => Promise<TProductDocument | null>;
}

const ProductSchema = new mongoose.Schema<IProductDocument, IProductModel>({
    title: {
        type: String,
        default: '',
    },
    slug: {
        type: String,
        required: true,
        minlength: 1,
    },
    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        default: undefined,
        validate: {
            validator: async (id?: mongoose.Types.ObjectId) => {
                if (!id) return true;

                // check productId is existed in Product table
                const product = await Category.exists({
                    _id: id,
                });

                if (!product) return false;
                return true;
            },
            message: (e: { value: mongoose.Types.ObjectId }) => {
                return `categoryId="${e.value.toString()}" does not exists in the Category table`;
            },
        },
    },
    summary: {
        type: String,
        default: '',
    },
    price: {
        type: Number,
        min: 0,
        default: 0,
    },
    isPromotionalPrice: {
        type: Boolean,
        default: false,
    },
    promotionPrice: {
        type: Number,
        default: 0,
    },
    promotionStartAt: {
        type: Date,
    },
    promotionEndAt: {
        type: Date,
    },
    image: {
        type: String,
        default: '',
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => {
            return new Date();
        },
    },
    updatedAt: {
        type: Date,
        default: () => {
            return new Date();
        },
    },
});

type TProductDocument = TLapisMongooseDocument<IProductDocument>;
export { IProductSchema, TProductDocument, IProductDocument, IProductModel };
export default ProductSchema;
