import mongoose from 'mongoose';
import lapisLog from '../../core/lapisLog';
import { ILapisGeneralSchema, TLapisMongooseDocument } from '../../core/LapisTypes';
import IMetaTag from '../../share_types/others/IMetaTag';
import ISpecification from '../../share_types/others/ISpecification';
import Category from '../models/Category';
import Product from '../models/Product';

interface IProductDetailSchema extends ILapisGeneralSchema {
    metaTag: IMetaTag[];
    images: string[];
    information: string;
    promotion: string;
    specifications: ISpecification[];
    description: string;
}

interface IProductDetailDocument extends IProductDetailSchema {}

interface IProductDetailModel extends mongoose.Model<IProductDetailDocument> {}

const ProductDetailSchema = new mongoose.Schema<IProductDetailDocument, IProductDetailModel>({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        validate: {
            validator: async (id: mongoose.Types.ObjectId) => {
                // check productId is existed in Product table
                const product = await Product.exists({
                    _id: id,
                });

                if (!product) return false;
                return true;
            },
            message: (e: { value: mongoose.Types.ObjectId }) => {
                return `_id="${e.value.toString()}" does not exists in the Product table`;
            },
        },
    },
    metaTag: [
        {
            name: {
                type: String,
                default: '',
            },
            content: {
                type: String,
                default: '',
            },
        },
    ],
    images: [
        {
            type: String,
        },
    ],
    information: {
        type: String,
        default: '',
    },
    promotion: {
        type: String,
        default: '',
    },
    specifications: [
        {
            name: {
                type: String,
                default: '',
            },
            value: {
                type: String,
                default: '',
            },
        },
    ],
    description: {
        type: String,
        default: '',
    },
    updatedAt: {
        type: Date,
        default: () => new Date(),
    },
    createdAt: {
        type: Date,
        immutable: true,
        default: () => new Date(),
    },
});

type TProductDetailDocument = TLapisMongooseDocument<IProductDetailDocument>;

export { IProductDetailSchema, IProductDetailDocument, IProductDetailModel, TProductDetailDocument };

export default ProductDetailSchema;
