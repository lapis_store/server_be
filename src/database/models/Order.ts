import mongoose from 'mongoose';
import OrderSchema, { IOrderDocument, IOrderModel } from '../schemas/OrderSchema';

OrderSchema.pre('save', async function (next) {
    const currentTime = new Date(Date.now());
    this.createdAt = currentTime;
    this.updatedAt = currentTime;
    next();
});

const Order = mongoose.model<IOrderDocument, IOrderModel>('order', OrderSchema);
export default Order;
