import mongoose from 'mongoose';
import ProductDetailSchema, { IProductDetailDocument, IProductDetailModel } from '../schemas/ProductDetailSchema';

ProductDetailSchema.pre('save', async function (next) {
    const currentTime = new Date(Date.now());
    this.updatedAt = currentTime;
    next();
});

const ProductDetail = mongoose.model<IProductDetailDocument, IProductDetailModel>('ProductDetail', ProductDetailSchema);

export default ProductDetail;
