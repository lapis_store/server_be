import mongoose from 'mongoose';
import lapisLog from '../../core/lapisLog';

import ProductSchema, {
    IProductSchema,
    TProductDocument,
    IProductModel,
    IProductDocument,
} from '../schemas/ProductSchema';

// VIRTUAL PROPERTIES

ProductSchema.virtual('currentPrice').get(function (this: IProductSchema): number {
    if (!this.isPromotionalPrice) return this.price;

    const currentTime = new Date();
    if (!this.promotionStartAt) {
        if (!this.promotionEndAt) return this.promotionPrice;
        if (currentTime < this.promotionEndAt) return this.promotionPrice;
        return this.price;
    }

    if (!this.promotionEndAt) {
        if (!this.promotionStartAt) return this.promotionPrice;
        if (this.promotionStartAt < currentTime) return this.promotionPrice;
        return this.price;
    }

    if (this.promotionStartAt < currentTime && currentTime < this.promotionEndAt) {
        return this.promotionPrice;
    }

    return this.price;
});

ProductSchema.virtual('isPromotion').get(function (this: IProductSchema): boolean {
    if (!this.isPromotionalPrice) return false;

    const currentTime = new Date();
    if (!this.promotionStartAt) {
        if (!this.promotionEndAt) return true;
        if (currentTime < this.promotionEndAt) return true;
        return false;
    }

    if (!this.promotionEndAt) {
        if (!this.promotionStartAt) return true;
        if (currentTime > this.promotionStartAt) return true;
        return false;
    }

    if (this.promotionStartAt < currentTime && currentTime < this.promotionEndAt) {
        return true;
    }

    return false;
});

// METHODS

ProductSchema.methods.slugIsExisted = async function () {
    if (!this.slug) return undefined;

    const product = await Product.findBySlug(this.slug);

    if (!product) return false;

    if (!this._id) return true;

    if (product._id.toString() === this._id.toString()) {
        return false;
    }
    return true;
};

// STATICS
ProductSchema.statics.findBySlug = async function (slug: string): Promise<TProductDocument | null> {
    return await Product.findOne({
        slug: slug,
    });
};

// EVENT
ProductSchema.pre('save', async function (next) {
    const currentTime = new Date(Date.now());
    this.updatedAt = currentTime;
    this.createdAt = currentTime;
    next();
});

// VALIDATOR
ProductSchema.path('slug').validate(async function (this: TProductDocument, value: string) {
    if (!this) return;
    const checkSlug = /^[a-z0-9\-]+$/;
    if (!checkSlug.test(this.slug)) return false;
    return !(await this.slugIsExisted());
}, `Invalid slug: slug include [a-z0-9\\-]{1,}`);

const Product = mongoose.model<IProductDocument, IProductModel>('Product', ProductSchema);

export default Product;
