import { NextFunction, Request, Response } from 'express';

import * as jwt from 'jsonwebtoken';
import ENV from '../core/ENV';
import WithToken, { ITokenPayload, WithTokenNotReadonly } from '../core/types/TRequestWithTokenPayload';

const authenticateToken = async (req: Request, res: Response, next: NextFunction) => {
    const accessToken = req.headers.authorization;

    if (!accessToken) {
        return res.status(401).send();
    }

    jwt.verify(accessToken, ENV.ACCESS_TOKEN_SECRET, (err, payload) => {
        if (err) {
            return res.status(403).send();
        }

        (req as WithTokenNotReadonly<Request>).tokenPayload = payload as ITokenPayload;
        next();
    });
};

export default authenticateToken;
