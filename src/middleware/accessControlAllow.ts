import express, { Request, Response, NextFunction } from 'express';
import lapisLog from '../core/lapisLog';
import { TLapisMiddleware } from '../core/LapisTypes';
import ENV from '../core/ENV';
import RegExpHub from '../core/RegExpHub';
import allowOrigins from '../config/allowOrigins';

const originPattern = /^(https?):\/\/([a-z0-9\.\-]+):?(\d+)?$/;

const getPort = (_port: string | undefined = undefined): number => {
    if (!_port) return 80;
    if (!RegExpHub.integer.test(_port)) return -1;

    const port = parseInt(_port);
    if (!isFinite(port)) return -1;

    return port;
};

const isAllow = (origin: string | undefined): boolean => {
    if (origin === undefined) return true; // same origin

    const matchResult = origin.match(originPattern);

    if (!matchResult) return false; // origin error

    const [
        _origin,
        _protocol,
        _domain,
        _port, // type = string|undefined
    ] = matchResult;

    const port = getPort(_port);
    if (port < 0) return false;

    // domain
    const allowOriginItem = allowOrigins.find((item) => item.domain === _domain);
    if (!allowOriginItem) return false;

    // protocol
    if (!allowOriginItem.protocol.includes(_protocol as any)) return false;

    // port
    if (!allowOriginItem.ports.includes(port)) return false;

    return true;
};

const accessControlAllow: TLapisMiddleware = async (req: Request, res: Response, next: NextFunction): Promise<any> => {
    const origin = req.headers.origin;

    if (!isAllow(origin)) {
        lapisLog('WARNING', `"${origin}" have been denied`);
        return res.status(403).send();
    }

    res.setHeader('Access-Control-Allow-Origin', origin || '');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    res.setHeader(
        'Access-Control-Allow-Headers',
        ['Origin', 'X-Requested-With', 'Content-Type', 'auth', 'authorization']
            .map((item) => item.toLowerCase())
            .join(', '),
    );

    res.setHeader('Access-Control-Allow-Credentials', 'true');

    if (req.method.toLocaleUpperCase() === 'OPTIONS') {
        return res.status(202).send();
    }

    next();

    //  // Website you wish to allow to connect
    //  res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:3000');

    //  // Request methods you wish to allow
    //  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    //  // Request headers you wish to allow
    //  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    //  // Set to true if you need the website to include cookies in the requests sent
    //  // to the API (e.g. in case you use sessions)
    //  res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
};

export default accessControlAllow;
