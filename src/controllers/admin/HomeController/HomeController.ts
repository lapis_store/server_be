import { Request, Response } from 'express';
import lapisLog from '../../../core/lapisLog';
import IHomeFormData from '../../../share_types/form_data/admin/IHomeFormData';
import IBadRequest from '../../../share_types/response/admin/IBadRequest';
import BaseHomeController from './BaseHomeController';

export default class HomeController extends BaseHomeController {
    private static _instance?: HomeController;

    public static get instance(): HomeController {
        if (!this._instance) this._instance = new HomeController();
        return this._instance;
    }

    protected constructor() {
        super();
    }

    // [POST] /create
    public create = async (req: Request, res: Response) => {
        const formData: IHomeFormData | null | undefined = req.body;

        if (!formData) {
            return res.status(400).send();
        }

        const validResult = await this.validateFormData(formData);
        if (validResult.status === 'invalid') {
            return res.status(400).json({
                message: validResult.message,
            } as IBadRequest);
        }

        try {
            const saveResult = await this.save(formData);
            if (!saveResult) {
                return res.status(500).send('save file failure');
            }
            return res.status(201).json(formData);
        } catch (e: { message: string } & any) {
            lapisLog('ERROR', e.message);
            return res.status(500).send();
        }
    };
}
