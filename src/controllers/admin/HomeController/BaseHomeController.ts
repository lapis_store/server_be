import IValidateResult from '../../../core/types/IValidateResult';
import IHomeFormData from '../../../share_types/form_data/admin/IHomeFormData';
import IBanner from '../../../share_types/others/IBanner';
import fs from 'fs';
import path from 'path';
import ENV from '../../../core/ENV';
import lapisLog from '../../../core/lapisLog';

export default class BaseHomeController {
    private readonly fileName: string = 'home.json';

    protected save = async (formData: IHomeFormData): Promise<boolean> => {
        return new Promise((resolve, reject) => {
            const filePath = path.join(ENV.DATA_DIR, this.fileName);
            try {
                const data = JSON.stringify(formData);
                fs.writeFileSync(filePath, data);
                resolve(true);
            } catch {
                lapisLog('ERROR', `[BaseHomeController] Can not save file to "${filePath}"`);
                resolve(false);
            }
        });
    };

    private validateBanner = (banner: IBanner): boolean => {
        if (Object.keys(banner).length !== 3) return false;

        const { alt, image, url } = banner;

        if (typeof alt !== 'string') return false;
        if (typeof image !== 'string') return false;
        if (typeof url !== 'string') return false;

        return true;
    };

    private validateBanners = (banners: IBanner[]): boolean => {
        for (const banner of banners) {
            if (!this.validateBanner(banner)) return false;
        }
        return true;
    };

    protected validateFormData = async (formData: IHomeFormData): Promise<IValidateResult> => {
        if (Object.keys(formData).length !== 4)
            return {
                status: 'invalid',
                message: 'Invalid formData',
            };

        const { carousel, bannersRightCarousel, bannersBelowCarousel, brand } = formData;

        if (!this.validateBanners(carousel))
            return {
                status: 'invalid',
                message: 'Invalid carousel',
            };

        if (!this.validateBanners(bannersRightCarousel))
            return {
                status: 'invalid',
                message: 'Invalid bannersRightCarousel',
            };

        if (!this.validateBanners(bannersBelowCarousel))
            return {
                status: 'invalid',
                message: 'Invalid bannersBelowCarousel',
            };

        if (!this.validateBanners(brand))
            return {
                status: 'invalid',
                message: 'Invalid brand',
            };

        return {
            status: 'valid',
            message: '',
        };
    };
}
