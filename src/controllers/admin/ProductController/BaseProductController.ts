import mongoose from 'mongoose';
import RegExpHub from '../../../core/RegExpHub';
import IValidateResult from '../../../core/types/IValidateResult';
import Product from '../../../database/models/Product';
import { TProductDocument } from '../../../database/schemas/ProductSchema';
import IProductFormData from '../../../share_types/form_data/admin/IProductFormData';

class BaseProductController {
    protected makeProduct = async (
        formData: IProductFormData,
        _product: TProductDocument | undefined = undefined,
    ): Promise<TProductDocument> => {
        const product = _product || new Product();
        product.title = formData.title || '';
        product.slug = formData.slug;

        if (!formData.categoryId || formData.categoryId === 'undefined') {
            product.categoryId = undefined;
        } else {
            product.categoryId = new mongoose.Types.ObjectId(formData.categoryId);
        }

        product.image = formData.image || '';
        product.price = formData.price || 0;

        if (!formData.isPromotionalPrice) {
            product.isPromotionalPrice = false;
        } else {
            product.isPromotionalPrice = true;
        }

        product.promotionPrice = formData.promotionPrice || 0;

        if (!formData.promotionStartAt || formData.promotionStartAt === 'undefined') {
            product.promotionStartAt = undefined;
        } else {
            product.promotionStartAt = new Date(formData.promotionStartAt);
        }

        if (!formData.promotionEndAt || formData.promotionEndAt === 'undefined') {
            product.promotionEndAt = undefined;
        } else {
            product.promotionEndAt = new Date(formData.promotionEndAt);
        }

        product.summary = formData.summary;
        return product;
    };

    protected validateFormData = async (formData: IProductFormData): Promise<IValidateResult> => {
        const {
            title,
            slug,
            categoryId,
            image,
            price,
            isPromotionalPrice,
            promotionPrice,
            promotionStartAt,
            promotionEndAt,
            keyword,
            summary,
        } = formData;

        // title
        if (typeof title !== 'string')
            return {
                status: 'invalid',
                message: 'Invalid title',
            };

        // slug
        if (typeof slug !== 'string')
            return {
                status: 'invalid',
                message: 'Invalid slug',
            };

        const slugReg = /^[a-z0-9\-]+$/i;
        if (!slugReg.test(slug))
            return {
                status: 'invalid',
                message: 'Invalid slug',
            };

        // category
        if (!['string', 'undefined'].includes(typeof categoryId))
            return {
                status: 'invalid',
                message: 'Invalid categoryId',
            };

        if (typeof categoryId === 'string' && !mongoose.Types.ObjectId.isValid(categoryId))
            return {
                status: 'invalid',
                message: 'Invalid categoryId',
            };

        // image
        if (typeof image !== 'string')
            return {
                status: 'invalid',
                message: 'Invalid image',
            };

        // price
        if (typeof price !== 'number')
            return {
                status: 'invalid',
                message: 'Invalid price',
            };

        if (price < 0)
            return {
                status: 'invalid',
                message: 'Invalid price',
            };

        // isPromotionalPrice
        if (!['boolean', 'undefined'].includes(typeof isPromotionalPrice))
            return {
                status: 'invalid',
                message: 'Invalid isPromotionalPrice',
            };

        // promotionPrice
        if (typeof promotionPrice !== 'number')
            return {
                status: 'invalid',
                message: 'Invalid promotionPrice',
            };

        if (promotionPrice < 0)
            return {
                status: 'invalid',
                message: 'Invalid price',
            };

        // promotionStartAt
        if (!['string', 'undefined'].includes(typeof promotionStartAt))
            return {
                status: 'invalid',
                message: 'Invalid promotionStartAt',
            };

        if (
            typeof promotionStartAt === 'string' &&
            !(promotionStartAt === 'undefined' || RegExpHub.date.test(promotionStartAt))
        )
            return {
                status: 'invalid',
                message: 'Invalid promotionStartAt',
            };

        // promotionEndAt
        if (!['string', 'undefined'].includes(typeof promotionEndAt))
            return {
                status: 'invalid',
                message: 'Invalid promotionEndAt',
            };

        if (
            typeof promotionEndAt === 'string' &&
            !(promotionEndAt === 'undefined' || RegExpHub.date.test(promotionEndAt))
        )
            return {
                status: 'invalid',
                message: 'Invalid promotionEndAt',
            };

        // summary
        if (typeof summary !== 'string')
            return {
                status: 'invalid',
                message: 'Invalid summary',
            };

        return {
            status: 'valid',
            message: '',
        };
    };
}

export default BaseProductController;
