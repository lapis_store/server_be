import { Request, Response } from 'express';
import mongoose from 'mongoose';
import CategoriesCache from '../../../cache/CategoriesCache';
import ProductsCache from '../../../cache/ProductsCache';
import lapisLog from '../../../core/lapisLog';
import Product from '../../../database/models/Product';
import IProductFormData from '../../../share_types/form_data/admin/IProductFormData';
import IBadRequest from '../../../share_types/response/admin/IBadRequest';
import BaseProductController from './BaseProductController';

class ProductController extends BaseProductController {
    private static _instance?: ProductController;
    public static get instance() {
        if (!this._instance) this._instance = new ProductController();
        return this._instance;
    }

    protected constructor() {
        super();
    }

    // [POST] /product/create
    public create = async (req: Request, res: Response) => {
        // get and check request body
        const formData: IProductFormData | undefined = req.body;
        if (!formData) {
            return res.status(400).json({
                message: 'FormData is undefined',
            } as IBadRequest);
        }

        const validResult = await this.validateFormData(formData);
        if (validResult.status === 'invalid') {
            return res.status(400).json({
                message: validResult.message,
            } as IBadRequest);
        }

        const product = await this.makeProduct(formData);
        if (!product) {
            return res.status(500).send(`Can't make product from formData`);
        }

        try {
            await product.save();
            ProductsCache.isLatest = false;
            return res.status(201).json(product.toObject());
        } catch {
            return res.status(500).send('Save product failure');
        }
    };

    // [PATCH] /product/update/:id
    public update = async (req: Request, res: Response) => {
        const productId: string | undefined = req.params.id;
        if (typeof productId !== 'string' || !mongoose.Types.ObjectId.isValid(productId)) {
            return res.status(404).send();
        }

        // get and check request body
        const formData: IProductFormData | undefined = req.body;
        if (!formData) {
            return res.status(400).json({
                message: 'FormData is undefined',
            } as IBadRequest);
        }

        const validResult = await this.validateFormData(formData);
        if (validResult.status === 'invalid') {
            return res.status(400).json({
                message: validResult.message,
            } as IBadRequest);
        }

        const tmpProduct = await Product.findById(productId);
        if (!tmpProduct) {
            return res.status(404).send();
        }

        const product = await this.makeProduct(formData, tmpProduct);
        if (!product) {
            return res.status(500).send(`Can't make product from formData`);
        }

        try {
            await product.save();
            ProductsCache.isLatest = false;
            return res.status(200).json(product.toObject());
        } catch {
            return res.status(500).send('Save product failure');
        }
    };

    // [DELETE] /product/remove/:id
    public remove = async (req: Request, res: Response) => {
        const productId: string | undefined = req.params.id;

        if (typeof productId !== 'string' || !mongoose.Types.ObjectId.isValid(productId)) {
            return res.status(404).send();
        }

        try {
            const product = await Product.findByIdAndDelete(productId);
            if (product) {
                ProductsCache.isLatest = false;
                return res.status(200).json({
                    _id: String(product._id.toString()),
                });
            }
            return res.status(404).send();
        } catch {
            return res.status(500).send();
        }
    };

    // [GET] /product/list
    public list = async (req: Request, res: Response) => {
        const categoryId = req.query.categoryId;

        const sortBy = -1;
        let products = (await ProductsCache.getInstance()).listProducts.map((item) => item.data);

        if (typeof categoryId === 'string' && mongoose.Types.ObjectId.isValid(categoryId)) {
            const categories = (await CategoriesCache.getInstance()).getAllChildren(categoryId);
            categories.add(categoryId);

            if (categories.size !== 0) {
                products = products.filter((product) => {
                    if (!product.categoryId) return false;
                    return categories.has(product.categoryId.toString());
                });
            }
        }

        products.sort((a, b) => {
            const aCreatedAt = a.createdAt;
            const bCreatedAt = b.createdAt;

            if (aCreatedAt > bCreatedAt) return 1 * sortBy;
            if (aCreatedAt < bCreatedAt) return -1 * sortBy;
            return 0;
        });

        return res.status(200).json(products);
    };

    // [GET] /product/find/:id
    public find = async (req: Request, res: Response) => {
        const productId: string | undefined = req.params.id;

        if (typeof productId !== 'string' || !mongoose.Types.ObjectId.isValid(productId)) {
            return res.status(404).send();
        }

        const product = (await ProductsCache.getInstance()).mapId.get(productId);
        if (!product) {
            return res.status(404).send();
        }

        return res.status(200).json(product.data);
    };

    // [GET] /product/image-name/:id
    public imageName = async (req: Request, res: Response) => {
        const productId: string | undefined = req.params.id;

        if (typeof productId !== 'string' || !mongoose.Types.ObjectId.isValid(productId)) {
            return res.status(404).send();
        }

        const product = (await ProductsCache.getInstance()).mapId.get(productId);
        if (!product) {
            return res.status(404).send();
        }

        return res.status(200).send(product.data.image);
    };

    // [GET] /product/check-slug
    public checkSlug = async (req: Request, res: Response) => {
        const slug: string | undefined = req.query.slug && String(req.query.slug);
        const productId: string | undefined = req.query.id && String(req.query.id);

        if (!['string', 'undefined'].includes(typeof productId)) {
            return res.status(400).send();
        }

        if (typeof slug !== 'string' || slug.length === 0) {
            return res.status(400).send();
        }

        const product = (await ProductsCache.getInstance()).mapSlug.get(slug);

        if (!product) {
            return res.status(200).json({
                isExisted: false,
            });
        }

        if (productId === product.id.toString()) {
            return res.status(200).json({
                isExisted: false,
            });
        }

        // response
        return res.status(200).json({
            isExisted: true,
        });
    };
}

export default ProductController;
