import express, { Request, Response, NextFunction } from 'express';
import mongoose from 'mongoose';
import CategoriesCache from '../../../cache/CategoriesCache';
import lapisLog from '../../../core/lapisLog';
import Category from '../../../database/models/Category';
import { ICategoryDocument, ICategorySchema } from '../../../database/schemas/CategorySchema';
import ICategoryFormData from '../../../share_types/form_data/admin/ICategoryFormData';
import IBadRequest from '../../../share_types/response/IBadRequest';
import BaseCategoryController from './BaseCategoryController';

class CategoryController extends BaseCategoryController {
    private static _instance?: CategoryController;
    public static get instance() {
        if (!this._instance) this._instance = new CategoryController();
        return this._instance;
    }

    protected constructor() {
        super();
    }

    // [POST] /create
    public create = async (req: Request, res: Response) => {
        const formData: ICategoryFormData = req.body;

        if (!formData) {
            return res.status(400).send();
        }

        // validation
        const validResult = await this.validFormData(formData);
        if (validResult.status === 'invalid') {
            return res.status(400).json({
                message: validResult.message,
            } as IBadRequest);
        }

        // create new category
        const category = await this.makeCategory(formData);

        // save
        try {
            await category.save();
            CategoriesCache.isLatest = false;
            return res.status(201).json(category);
        } catch (e: any & { message: string }) {
            lapisLog('ERROR', `Can't save category: ${e.message}`);
            return res.status(500).send();
        }
    };

    // [PATCH] /update/:id
    public update = async (req: Request, res: Response, next: NextFunction) => {
        const categoryId: string = req.params.id;

        // Get category from client
        const formData: ICategoryFormData = req.body;
        if (!formData) {
            return res.status(400).send();
        }

        const validResult = await this.validFormData(formData);
        if (validResult.status === 'invalid') {
            return res.status(400).json({
                message: validResult.message,
            } as IBadRequest);
        }

        // Find category and check
        let category = await Category.findById(categoryId);
        if (!category || category === null) {
            return res.status(404).send();
        }

        category = await this.makeCategory(formData, category);

        // Check if fullPath of category is endless "circular loops"
        if (await category.fullPathIsCircularLoops()) {
            return res.status(400).json({
                message: `Invalid parentId !. parentId can't contain child id`,
            });
        }

        // Save
        try {
            await category.save();
            CategoriesCache.isLatest = false;
            return res.status(200).json(category);
        } catch (e: any & { message: string }) {
            lapisLog('ERROR', `Can't save category: ${e.message}`);
            return res.status(400).send();
        }
    };

    // [DELETE] /category/remove/:id
    public remove = async (req: Request, res: Response, next: NextFunction) => {
        const categoryId: string = req.params.id;

        try {
            // find
            const category = await Category.findByIdAndRemove(categoryId);

            // response
            if (!category || category === null) {
                return res.status(404).send();
            }

            const children = await Category.find({ parentId: category.id });
            for (const child of children) {
                child.parentId = undefined;
                await child.save();
            }

            CategoriesCache.isLatest = false;
            return res.status(200).json(category.toObject());
        } catch (e: any & { message: string }) {
            lapisLog('ERROR', e.message);
        }

        return res.status(404).send();
    };

    // [DELETE] /category/removeAll/:id
    public removeAll = async (req: Request, res: Response, next: NextFunction) => {
        const categoryId: string = req.params.id;

        try {
            // find
            const category = await Category.findByIdAndRemove(categoryId);

            // response
            if (!category || category === null) {
                return res.status(404).send();
            }

            const removeAllChildren = async (parentId: string | mongoose.Types.ObjectId) => {
                const children = await Category.find({ parentId: parentId });
                if (children.length === 0) return;

                for (const child of children) {
                    await removeAllChildren(child._id);
                    child.remove();
                }
            };

            await removeAllChildren(category.id);

            CategoriesCache.isLatest = false;
            return res.status(200).json(category.toObject());
        } catch (e: any & { message: string }) {
            lapisLog('ERROR', e.message);
        }

        return res.status(404).send();
    };

    // [GET] /category/find/:id
    public find = async (req: Request, res: Response, next: NextFunction) => {
        const categoryId: string = req.params.id;

        // find
        const category = await Category.findById(categoryId);

        // response
        if (!category) {
            return res.status(404).send();
        }

        return res.status(200).json({
            ...category.toObject(),
            fullPath: await category.fullPath,
        } as ICategoryDocument);
    };

    // [GET] /list
    public list = async (req: Request, res: Response) => {
        const categories = (await CategoriesCache.getInstance()).data;
        return res.status(200).json(categories);
    };
}

export default CategoryController;
