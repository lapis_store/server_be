import mongoose from 'mongoose';
import IValidateResult from '../../../core/types/IValidateResult';
import Category from '../../../database/models/Category';
import { TCategoryDocument } from '../../../database/schemas/CategorySchema';
import ICategoryFormData from '../../../share_types/form_data/admin/ICategoryFormData';

class BaseCategoryController {
    protected constructor() {}

    protected validFormData = async (formData: ICategoryFormData): Promise<IValidateResult> => {
        const { title, image, parentId, index } = formData;

        if (typeof index !== 'number' || !isFinite(index)) {
            return {
                status: 'invalid',
                message: 'Invalid index',
            };
        }

        // title
        if (typeof title !== 'string') {
            return {
                status: 'invalid',
                message: 'Invalid title',
            };
        }

        // image
        if (!['undefined', 'string'].includes(typeof image)) {
            return {
                status: 'invalid',
                message: 'Invalid image',
            };
        }

        // parentId
        if (!['undefined', 'string'].includes(typeof parentId)) {
            return {
                status: 'invalid',
                message: 'Invalid parentId',
            };
        }

        if (typeof parentId === 'string' && parentId !== 'undefined') {
            if (!mongoose.Types.ObjectId.isValid(parentId))
                return {
                    status: 'invalid',
                    message: 'Invalid parentId',
                };

            const isExists = await Category.exists({ _id: parentId });
            if (!isExists)
                return {
                    status: 'invalid',
                    message: 'parentId not exists',
                };
        }

        return {
            status: 'valid',
            message: '',
        };
    };

    protected makeCategory = async (formData: ICategoryFormData, _category?: TCategoryDocument) => {
        const category = _category || new Category();

        category.title = formData.title;
        category.index = formData.index;
        category.image = formData.image;

        if (typeof formData.parentId === 'undefined' || formData.parentId === 'undefined') {
            category.parentId = undefined;
        } else {
            category.parentId = new mongoose.Types.ObjectId(formData.parentId);
        }

        return category;
    };
}

export default BaseCategoryController;
