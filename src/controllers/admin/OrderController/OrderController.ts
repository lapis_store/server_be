import { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose';
import lapisLog from '../../../core/lapisLog';
import Order from '../../../database/models/Order';
import EOrderStatus from '../../../database/types/EOrderStatus';
import calc from '../../../functions/calc';
import IOrderResponse, {
    IOrderItemResponse as IOrderItemResponse,
} from '../../../share_types/response/admin/order/TListOrderResponse';
import BaseOrderController from './BaseOrderController';

class OrderController extends BaseOrderController {
    private static _instance?: OrderController;

    public static get instance(): OrderController {
        if (!this._instance) this._instance = new OrderController();
        return this._instance;
    }

    public constructor() {
        super();
    }

    public baseStatistics = async (req: Request, res: Response) => {
        const orders = await Order.find(
            {},
            {
                total: 1,
                createdAt: 1,
            },
        );

        const maxDate: string = new Date().toJSON();
        let minDate: string = maxDate;

        orders.forEach((order) => {
            if (minDate > order.createdAt.toJSON()) {
                minDate = order.createdAt.toJSON();
            }
        });

        const totalEarned: number = orders.reduce((total, order) => {
            total += order.total;
            return total;
        }, 0);

        const totalOrdersPlaced: number = orders.length;

        const numberOfDates: number = (() => {
            const _numberOfDates = calc.LapisDate.millisecondsToNumberOfDates(
                new Date(maxDate).getTime() - new Date(minDate).getTime(),
            );

            // avoid divide by 0
            if (_numberOfDates < 1) return 1;

            return _numberOfDates;
        })();

        const averageEarnedInDays = Math.round((totalEarned / numberOfDates) * 100) / 100;
        const averageOrdersPlacedInDays = Math.round((totalOrdersPlaced / numberOfDates) * 100) / 100;

        return res.status(200).json({
            totalEarned: totalEarned || 0,
            totalOrdersPlaced: totalOrdersPlaced || 0,
            averageEarnedInDays: averageEarnedInDays || 0,
            averageOrdersPlacedInDays: averageOrdersPlacedInDays || 0,
        });
    };

    public list = async (req: Request, res: Response) => {
        const startAt = this.getTimeQuery(req.query.startAt) || calc.LapisDate.plus(-30); // 30 day ago
        const endAt = this.getTimeQuery(req.query.endAt) || calc.LapisDate.plus(1);

        const orders = await Order.find({
            createdAt: {
                $gte: startAt,
                $lte: endAt,
            },
        }).sort({ createdAt: -1 });

        const ordersResponse: IOrderResponse[] = [];
        for (const order of orders) {
            const orderResponse = await this.makeOrderResponse(order);
            if (!orderResponse) return;
            ordersResponse.push(orderResponse);
        }

        res.status(200).json(ordersResponse);
        res.end();
        return;
    };

    // [PATCH] /update-status/:orderId
    public updateStatus = async (req: Request, res: Response) => {
        const orderId = req.params.orderId;
        const orderStatus: EOrderStatus = req.body.orderStatus;

        if (typeof orderStatus !== 'string' || !Object.values(EOrderStatus).includes(orderStatus)) {
            return res.status(400).send();
        }

        if (!orderId || !mongoose.Types.ObjectId.isValid(orderId)) {
            return res.status(400).send();
        }

        const order = await Order.findById(orderId);
        if (!order) {
            return res.status(404).send();
        }

        order.orderStatus = orderStatus;

        try {
            await order.save();
            return res.status(200).json(await this.makeOrderResponse(order));
        } catch {
            return res.status(500).send();
        }
    };

    // [GET] /find/:id
    public find = async (req: Request, res: Response) => {
        const id = req.params.id;
        if (!id || !mongoose.Types.ObjectId.isValid(id)) {
            return res.status(404).send();
        }

        const orders = await Order.findById(new mongoose.Types.ObjectId(id));
        if (!orders) {
            return res.status(404).send();
        }

        res.status(200).json(orders.toJSON());
        return;
    };
}

export default OrderController;
