import RegExpHub from '../../../core/RegExpHub';
import { IOrderDocument, TOrderDocument } from '../../../database/schemas/OrderSchema';
import calc from '../../../functions/calc';
import IOrderResponse, { IOrderItemResponse } from '../../../share_types/response/admin/order/TListOrderResponse';

class BaseOrderController {
    protected constructor() {}

    protected makeKeyByDay(d: Date): string {
        const dd = d.getDate().toString().padStart(2, '0');
        const mm = d.getMonth().toString().padStart(2, '0');
        const yyyy = d.getFullYear().toString();

        return `${yyyy}-${mm}-${dd}`;
    }

    protected getTimeQuery = (property: any): Date | undefined => {
        if (typeof property !== 'string') return undefined;
        if (!RegExpHub.date.test(property)) return undefined;

        return new Date(property);
    };

    protected makeOrderResponse = async (doc: TOrderDocument) => {
        if (!doc) return undefined;

        return {
            _id: doc._id.toString(),
            customerName: doc.customerName,
            phoneNumber: doc.phoneNumber,
            address: doc.address,
            total: doc.total,
            orderStatus: doc.orderStatus,
            createdAt: doc.createdAt.toJSON(),
            updatedAt: doc.updatedAt.toJSON(),
            orderItems: doc.orderItems.map((orderItem) => {
                return {
                    productId: orderItem.productId.toString(),
                    productName: orderItem.productName,
                    price: orderItem.price,
                    quantity: orderItem.quantity,
                } as IOrderItemResponse;
            }),
        } as IOrderResponse;
    };
}

export default BaseOrderController;
