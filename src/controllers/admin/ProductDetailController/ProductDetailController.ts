import { Request, Response } from 'express';
import mongoose from 'mongoose';
import lapisLog from '../../../core/lapisLog';
import ProductDetail from '../../../database/models/ProductDetail';
import IProductDetailFormData from '../../../share_types/form_data/admin/IProductDetailFormData';
import IBadRequest from '../../../share_types/response/admin/IBadRequest';
import BaseProductDetailController from './BaseProductDetailController';

class ProductDetailController extends BaseProductDetailController {
    private static _instance?: ProductDetailController;
    public static get instance() {
        if (!this._instance) this._instance = new ProductDetailController();
        return this._instance;
    }

    protected constructor() {
        super();
    }

    // [POST] /create
    public create = async (req: Request, res: Response) => {
        const formData: IProductDetailFormData = req.body;

        if (typeof formData !== 'object') {
            return res.status(400).json({
                message: 'Invalid request formData',
            } as IBadRequest);
        }

        // valid
        const validResult = await this.validateFormData(formData);
        if (validResult.status === 'invalid') {
            return res.status(400).json({
                message: validResult.message,
            } as IBadRequest);
        }

        // make
        const productDetail = await this.makeProductDetail(formData);

        // save
        try {
            await productDetail.save();
            res.status(201).json(productDetail);
        } catch (e: { message: string } & any) {
            lapisLog('ERROR', e.message);
            return res.status(500).send();
        }
    };

    // [PATCH] update
    public update = async (req: Request, res: Response) => {
        const formData: IProductDetailFormData = req.body;

        if (typeof formData !== 'object') {
            return res.status(400).json({
                message: 'Invalid request formData',
            } as IBadRequest);
        }

        // valid
        const validResult = await this.validateFormData(formData);
        if (validResult.status === 'invalid') {
            return res.status(400).json({
                message: validResult.message,
            } as IBadRequest);
        }

        // make
        const tmpProductDetail = await ProductDetail.findById(formData._id);
        if (!tmpProductDetail) {
            return res.status(404).send();
        }
        const productDetail = await this.makeProductDetail(formData, tmpProductDetail);

        // save
        try {
            await productDetail.save();
            res.status(200).json(productDetail);
        } catch (e: { message: string } & any) {
            lapisLog('ERROR', e.message);
            return res.status(500).send();
        }
    };

    // [GET] /find/:productId
    public find = async (req: Request, res: Response) => {
        const productId: string | undefined = req.params.id;
        if (typeof productId !== 'string' || !mongoose.Types.ObjectId.isValid(productId)) {
            return res.status(404).send();
        }

        const productDetail = await ProductDetail.findById(productId);

        // not found
        if (!productDetail) {
            return res.status(404).send();
        }

        res.status(200).json(productDetail);
    };
}

export default ProductDetailController;
