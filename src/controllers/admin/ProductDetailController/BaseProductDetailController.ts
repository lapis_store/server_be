import mongoose from 'mongoose';
import ProductsCache from '../../../cache/ProductsCache';
import IValidateResult from '../../../core/types/IValidateResult';
import ProductDetail from '../../../database/models/ProductDetail';
import { TProductDetailDocument } from '../../../database/schemas/ProductDetailSchema';
import IProductDetailFormData from '../../../share_types/form_data/admin/IProductDetailFormData';

class BaseProductDetailController {
    protected makeProductDetail = async (
        formData: IProductDetailFormData,
        _productDetail: TProductDetailDocument = null,
    ) => {
        const productDetail = _productDetail || new ProductDetail();
        productDetail._id = new mongoose.Types.ObjectId(formData._id);
        productDetail.images = formData.images || [];
        productDetail.information = formData.information || '';
        productDetail.promotion = formData.promotion || '';
        productDetail.specifications = formData.specifications || [];
        productDetail.description = formData.description || '';
        return productDetail;
    };

    protected validateFormData = async (formData: IProductDetailFormData): Promise<IValidateResult> => {
        const { _id, images, information, promotion, specifications, description } = formData;

        // id
        const validateId = async () => {
            if (typeof _id !== 'string') return false;
            if (!mongoose.Types.ObjectId.isValid(_id)) return false;
            if (!(await ProductsCache.getInstance()).mapId.has(_id)) return false;
            return true;
        };
        if (!(await validateId()))
            return {
                status: 'invalid',
                message: 'Invalid id',
            };

        // images
        const validateImages = () => {
            if (typeof images === 'undefined') return true;
            if (!Array.isArray(images)) return false;

            for (const item in images) {
                if (typeof item !== 'string') return false;
            }
            return true;
        };
        if (!validateImages())
            return {
                status: 'invalid',
                message: 'Invalid images',
            };

        // information
        if (!['undefined', 'string'].includes(typeof information))
            return {
                status: 'invalid',
                message: 'Invalid information',
            };

        // promotion
        if (!['undefined', 'string'].includes(typeof promotion))
            return {
                status: 'invalid',
                message: 'Invalid promotion',
            };

        // specifications
        const validateSpecifications = () => {
            if (typeof specifications === 'undefined') return true;

            if (!Array.isArray(specifications)) return false;

            for (const item of specifications) {
                if (Object.keys(item).length !== 2) return false;
                if (typeof item.name !== 'string') return false;
                if (typeof item.value !== 'string') return false;
            }
            return true;
        };
        if (!validateSpecifications())
            return {
                status: 'invalid',
                message: 'Invalid specifications',
            };

        // description
        if (!['undefined', 'string'].includes(typeof description))
            return {
                status: 'invalid',
                message: 'Invalid description',
            };

        return {
            status: 'valid',
            message: '',
        };
    };
}

export default BaseProductDetailController;
