import { NextFunction, Request, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import ENV from '../../../core/ENV';
import WithToken, { ITokenPayload } from '../../../core/types/TRequestWithTokenPayload';
import IAdminLoginResponse from '../../../share_types/response/login/IAdminLoginResponse';

import BaseAdminLoginController from './BaseAdminLoginController';

class AdminLoginController extends BaseAdminLoginController {
    private static _instance?: AdminLoginController;

    public static get instance(): AdminLoginController {
        if (!this._instance) this._instance = new AdminLoginController();
        return this._instance;
    }

    protected constructor() {
        super();
    }

    // [POST] /signIn
    public signIn = async (req: Request, res: Response) => {
        const { userName, password } = req.body;

        if (!userName || !password) {
            return res.status(200).json({
                status: 'failure',
            } as IAdminLoginResponse);
        }

        // check if logged
        const requestTokenPayload = (req as WithToken<Request>).tokenPayload;

        if (requestTokenPayload && requestTokenPayload.userId.length !== 0) {
            return res.status(200).json({
                status: 'successfully',
            } as IAdminLoginResponse);
        }

        // check
        const isLoginSuccessfully = await this.check(userName, password);

        // failure
        if (!isLoginSuccessfully) {
            return res.status(200).json({
                status: 'failure',
            } as IAdminLoginResponse);
        }

        // successfully
        const tokenPayload: ITokenPayload = {
            userId: (3451245421).toString(16),
        };

        const accessToken = jwt.sign(tokenPayload, ENV.ACCESS_TOKEN_SECRET);

        return res.status(200).json({
            status: 'successfully',
            accessToken,
        } as IAdminLoginResponse);
    };
}

export default AdminLoginController;
