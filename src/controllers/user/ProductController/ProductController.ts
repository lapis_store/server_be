import express, { Request, Response, NextFunction } from 'express';
import mongoose from 'mongoose';
import CategoriesCache from '../../../cache/CategoriesCache';
import ProductsCache from '../../../cache/ProductsCache';
import lapisLog from '../../../core/lapisLog';
import ProductDetail from '../../../database/models/ProductDetail';
import IProductRes from '../../../share_types/response/user/IProductRes';

export default class ProductController {
    private static _instance?: ProductController;
    public static get instance() {
        if (!this._instance) this._instance = new ProductController();
        return this._instance;
    }

    public constructor() {}

    public index = async (req: Request, res: Response, next: NextFunction) => {
        // res.status(200).send('Setup home controller successfully');
    };

    // [GET] /product/list
    public list = async (req: Request, res: Response) => {
        const categoryId = req.query.categoryId;

        const sortBy = -1;
        let products = (await ProductsCache.getInstance()).listProducts;

        if (typeof categoryId === 'string' && mongoose.Types.ObjectId.isValid(categoryId)) {
            const categories = (await CategoriesCache.getInstance()).getAllChildren(categoryId);
            categories.add(categoryId);

            if (categories.size !== 0) {
                products = products.filter((product) => {
                    if (!product.data.categoryId) return false;
                    return categories.has(product.data.categoryId.toString());
                });
            }
        }

        products.sort((a, b) => {
            const aCreatedAt = a.data.createdAt;
            const bCreatedAt = b.data.createdAt;

            if (aCreatedAt > bCreatedAt) return 1 * sortBy;
            if (aCreatedAt < bCreatedAt) return -1 * sortBy;
            return 0;
        });

        return res.status(200).json(products.map((item) => item.toResObject()));
    };

    public latestProducts = async (req: Request, res: Response) => {
        const products = (await ProductsCache.getInstance()).listProducts.map((item) => item.toResObject());
        return res.status(200).json(products);
    };

    // [GET] /get-by-id/:id
    public getById = async (req: Request, res: Response, next: NextFunction) => {
        const id = req.params.id && String(req.params.id);
        if (!id) return res.status(404).send();

        const product = (await ProductsCache.getInstance()).mapId.get(id);
        if (!product) {
            return res.status(404).send();
        }

        return res.status(200).json(product.toResObject());
    };

    // [GET] /get-by-list-id
    public getByListId = async (req: Request, res: Response, next: NextFunction) => {
        const listId = req.query.listId;

        if (!listId || !Array.isArray(listId)) {
            return res.status(404).send();
        }

        const mapProducts = (await ProductsCache.getInstance()).mapId;

        const resData: IProductRes[] = [];
        listId.forEach((id) => {
            const _id = String(id);
            const product = mapProducts.get(_id);

            if (!product) return;

            resData.push(product.toResObject());
        });

        return res.status(200).json(resData);
    };

    public getBySlug = async (req: Request, res: Response, next: NextFunction) => {
        const slug = req.query.slug;
        if (!slug || typeof slug !== 'string') {
            res.status(404).send();
            res.end();
            return;
        }

        const product = (await ProductsCache.getInstance()).mapSlug.get(slug);
        if (!product) {
            res.status(404).send();
            res.end();
            return;
        }

        const productDetail = await ProductDetail.findOne(
            {
                _id: new mongoose.Types.ObjectId(product.id),
            },
            {
                _id: 1,
                metaTag: 1,
                images: 1,
                information: 1,
                promotion: 1,
                promotionMore: 1,
                specifications: 1,
                description: 1,
            },
        );

        const resData = product.toResObject();
        return res.status(200).json({
            product: resData,
            productDetail: productDetail && productDetail.toObject(),
        });
    };
}
