import { Request, Response, NextFunction } from 'express';
import CategoriesCache from '../../../cache/CategoriesCache';
import Category from '../../../database/models/Category';
import { ICategoryDocument, ICategorySchema, TCategoryDocument } from '../../../database/schemas/CategorySchema';

export default class CategoryController {
    private static _instance?: CategoryController;
    public static get instance() {
        if (!this._instance) this._instance = new CategoryController();
        return this._instance;
    }

    public index = async (req: Request, res: Response, next: NextFunction) => {};

    // [GET] /list
    public list = async (req: Request, res: Response, next: NextFunction) => {
        // cache
        // if(this.categoriesCache){
        //     return res.status(200).json(this.categoriesCache);
        // }

        // const categories = await Category.find();

        // const categoriesRes = categories.map((category) => {
        //     return category.toObject();
        // });

        // this.categoriesCache = categoriesRes;

        return res.status(200).json((await CategoriesCache.getInstance()).data);
    };

    // [GET] /find/:id
    public find = async (req: Request, res: Response) => {
        const id = req.params.id;

        if (typeof id !== 'string') {
            return res.status(404).send();
        }

        const category = (await CategoriesCache.getInstance()).data.find((item) => item._id.toString() === id);

        if (!category) {
            return res.status(404).send();
        }

        return res.status(200).json(category);
    };
}
