import express, { Request, Response, NextFunction } from 'express';
import ProductsCache from '../../../cache/ProductsCache';
import lapisLog from '../../../core/lapisLog';
import removeVietnameseAccents from '../../../functions/removeVietnameseAccents';
import standardizationKeyword from '../../../functions/standardizationKeyword';
import IProductRes from '../../../share_types/response/user/IProductRes';

export default class SearchController {
    private static _instance?: SearchController;
    public static get instance() {
        if (!this._instance) this._instance = new SearchController();
        return this._instance;
    }

    public maxSuggestionsRes: number = 7;

    public index = async (req: Request, res: Response, next: NextFunction) => {
        const keyword = req.query.keyword && String(req.query.keyword);

        if (!keyword) {
            lapisLog('ERROR', 'suggestions with q undefined', keyword);
            res.status(404).send();
            res.end();
            return;
        }

        const keywordReg = new RegExp(standardizationKeyword(keyword), 'i');
        const suggestions = (await ProductsCache.getInstance()).suggestions;

        const resData: IProductRes[] = [];

        for (let item of suggestions) {
            if (keywordReg.test(item.key)) {
                resData.push(item.product.toResObject());
            }

            if (resData.length >= 40) {
                break;
            }
        }

        resData.sort((a, b) => {
            const aTitle = a.title.toLowerCase();
            const bTitle = b.title.toLowerCase();

            if (aTitle > bTitle) return 1;
            if (aTitle < bTitle) return -1;
            return 0;
        });

        res.status(200).json(resData);
        res.end();
    };

    // [GET] /suggestions/q=
    public suggestions = async (req: Request, res: Response, next: NextFunction) => {
        const q = req.query.q && String(req.query.q);

        if (!q) {
            lapisLog('ERROR', 'suggestions with q undefined', q);
            res.status(404).send();
            res.end();
            return;
        }

        const keywordReg = new RegExp(standardizationKeyword(q), 'i');
        const suggestions = (await ProductsCache.getInstance()).suggestions;

        const suggestionsRes: string[] = [];
        for (let item of suggestions) {
            if (keywordReg.test(item.key)) {
                suggestionsRes.push(item.product.data.title);
            }

            if (suggestionsRes.length >= this.maxSuggestionsRes) {
                break;
            }
        }

        suggestionsRes.sort();
        res.status(200).json(suggestionsRes);
        res.end();
    };
}
