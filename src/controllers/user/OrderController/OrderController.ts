import { NextFunction, Request, Response } from 'express';
import lapisLog from '../../../core/lapisLog';
import Order from '../../../database/models/Order';
import IOrderFormData from '../../../share_types/form_data/user/IOrderFormData';
import IBadRequest from '../../../share_types/response/IBadRequest';
import IAddOrderRes from '../../../share_types/response/user/order/IAddOrderRes';
import BaseOrderController from './BaseOrderController';

class OrderController extends BaseOrderController {
    private static _instance?: OrderController;

    public static get instance(): OrderController {
        if (!this._instance) this._instance = new OrderController();
        return this._instance;
    }

    public constructor() {
        super();
    }

    // [POST] /add
    public add = async (req: Request, res: Response, next: NextFunction) => {
        const formData: IOrderFormData | undefined = req.body;

        if (!formData) return res.status(400).send();

        const validResult = await this.validateFormData(formData);
        if (validResult.status === 'invalid') {
            res.status(400).json({
                message: validResult.message,
            } as IBadRequest);

            return res.end();
        }

        const orderDocument = await this.makeOrderDocument(formData);
        if (!orderDocument) {
            res.status(400).json({
                message: 'Create order document failed',
            } as IBadRequest);

            return res.end();
        }

        try {
            // save
            const order = new Order(orderDocument);
            await order.save();

            const resOrder = order.toJSON();

            const resData: IAddOrderRes = {
                _id: resOrder._id.toString(),
                customerName: resOrder.customerName,
                phoneNumber: resOrder.phoneNumber,
                address: resOrder.address,
                total: resOrder.total,
                orderItems: resOrder.orderItems.map((item) => {
                    return {
                        productId: item.productId.toString(),
                        productName: item.productName,
                        price: item.price,
                        quantity: item.quantity,
                    };
                }),
            };

            res.status(201).send(resData);
            res.end();
        } catch (e) {
            lapisLog('ERROR', (e as any).message);
            res.status(500).send();
            res.end();
        }
    };
}

export default OrderController;
