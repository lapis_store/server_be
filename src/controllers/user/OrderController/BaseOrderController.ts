import mongoose from 'mongoose';
import ProductsCache from '../../../cache/ProductsCache';
import lapisLog from '../../../core/lapisLog';
import RegExpHub from '../../../core/RegExpHub';
import IValidateResult from '../../../core/types/IValidateResult';
import { IOrderDocument } from '../../../database/schemas/OrderSchema';
import IOrderItem from '../../../database/types/IOrderItem';
import IOrderFormData from '../../../share_types/form_data/user/IOrderFormData';

class BaseOrderController {
    protected constructor() {}

    protected validateFormData = async (formData: IOrderFormData): Promise<IValidateResult> => {
        if (!formData)
            return {
                status: 'invalid',
                message: 'Invalid form data',
            };

        const { customerName, phoneNumber, address, orderItems, email } = formData;

        // customer name
        if (typeof customerName !== 'string' || customerName.length === 0)
            return {
                status: 'invalid',
                message: `Invalid customer name value="${customerName}"`,
            };

        // phone number
        if (typeof phoneNumber !== 'string' || !RegExpHub.phoneNumber.test(phoneNumber))
            return {
                status: 'invalid',
                message: 'Invalid phoneNumber',
            };

        // address
        if (typeof address !== 'string' || address.length == 0)
            return {
                status: 'invalid',
                message: 'Invalid address',
            };

        // order items
        if (!Array.isArray(orderItems) || orderItems.length === 0)
            return {
                status: 'invalid',
                message: 'Invalid order items',
            };

        for (let { productId, quantity } of orderItems) {
            if (typeof productId !== 'string' || !RegExpHub.objectId.test(productId))
                return {
                    status: 'invalid',
                    message: 'Some item in order are not valid',
                };

            if (typeof quantity !== 'number' || quantity <= 0)
                return {
                    status: 'invalid',
                    message: 'Some quantity of item in order are not valid',
                };
        }

        return {
            status: 'valid',
            message: '',
        };
    };

    protected makeOrderDocument = async (formData: IOrderFormData): Promise<IOrderDocument | undefined> => {
        const { customerName, phoneNumber, address, email } = formData;

        const mapProduct = (await ProductsCache.getInstance()).mapId;

        //
        const orderItems: IOrderItem[] = [];
        for (let item of formData.orderItems) {
            const product = mapProduct.get(item.productId);

            if (!product) {
                lapisLog(
                    'WARNING',
                    `Items in orderItems have productId="${item.productId}" not exists in current list product`,
                );
                return undefined;
            }

            orderItems.push({
                productId: new mongoose.Types.ObjectId(item.productId),
                productName: product.data.title,
                price: product.currentPrice,
                quantity: item.quantity,
            });
        }

        const total: number = orderItems.reduce((_total, currentItem) => {
            const { price, quantity } = currentItem;
            return _total + price * quantity;
        }, 0);

        const orderDocument = {
            customerName,
            phoneNumber,
            address,
            email,
            orderItems,
            total,
        } as IOrderDocument;

        return orderDocument;
    };
}

export default BaseOrderController;
