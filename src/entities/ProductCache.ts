import mongoose from 'mongoose';
import lapisLog from '../core/lapisLog';
import { IProductDocument, IProductSchema, TProductDocument } from '../database/schemas/ProductSchema';
import IProductRes from '../share_types/response/user/IProductRes';

export default class ProductCache {
    private _data: mongoose.LeanDocument<
        mongoose.Document<unknown, any, IProductDocument> &
            IProductDocument & {
                _id: string | mongoose.Types.ObjectId;
            }
    >;

    public constructor(data: TProductDocument) {
        if (!data) {
            lapisLog('ERROR', `data for ProductCache is null or undefined`);
            throw new Error('data for ProductCache is null or undefined');
        }

        this._data = data.toObject();
    }

    public get isPromotion(): boolean {
        if (!this._data.isPromotionalPrice) return false;

        const currentTime = new Date();

        if (!this._data.promotionStartAt) {
            if (!this._data.promotionEndAt) return true;
            if (currentTime < this._data.promotionEndAt) return true;
            return false;
        }

        if (!this._data.promotionEndAt) {
            if (!this._data.promotionStartAt) return true;
            if (currentTime > this._data.promotionStartAt) return true;
            return false;
        }

        if (this._data.promotionStartAt < currentTime && currentTime < this._data.promotionEndAt) {
            return true;
        }

        return false;
    }

    public get currentPrice(): number {
        if (this.isPromotion && this._data.promotionPrice !== undefined) return this._data.promotionPrice;

        return this._data.price;
    }

    public get id() {
        return this._data._id;
    }

    public get data() {
        return this._data;
    }

    public toResObject(): IProductRes {
        const isPromotion = this.isPromotion;
        return {
            _id: this._data._id.toString(),
            slug: this._data.slug,
            image: this._data.image,
            title: this._data.title,
            isPromotion,
            price: this._data.price,
            promotionPrice: isPromotion ? this._data.promotionPrice : undefined,
            summary: this._data.summary,
        } as IProductRes;
    }
}
