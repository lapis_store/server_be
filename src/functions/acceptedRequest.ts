import { Request, Response } from 'express';

const acceptedRequest = async (req: Request, res: Response) => {
    res.status(200).send();
    // res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization, auth");
    res.end();
};

export default acceptedRequest;
