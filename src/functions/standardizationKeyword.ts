import removeVietnameseAccents from './removeVietnameseAccents';

const standardizationKeyword = (keyword: string) => {
    let result = keyword.replace(/[\s]+/g, ' ').trim().toLowerCase();
    return removeVietnameseAccents(result);
};

export default standardizationKeyword;
