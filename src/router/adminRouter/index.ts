import express from 'express';

import productDetailRouter from './productDetailRouter';
import categoryRouter from './categoryRouter';
import productRouter from './productRouter';
import orderRouter from './orderRouter';
import homeRouter from './homeRouter';

const adminRouter = express.Router();

adminRouter.use('/product', productRouter);
adminRouter.use('/category', categoryRouter);
adminRouter.use('/product-detail', productDetailRouter);
adminRouter.use('/order', orderRouter);
adminRouter.use('/home', homeRouter);

export default adminRouter;
