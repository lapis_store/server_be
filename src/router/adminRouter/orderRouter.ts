import express from 'express';
import OrderController from '../../controllers/admin/OrderController';
// import acceptedRequest from '../../functions/acceptedRequest';

const orderController = OrderController.instance;
const orderRouter = express.Router();

orderRouter.get('/find/:id', orderController.find);
orderRouter.get('/list', orderController.list);
orderRouter.get('/base-statistics', orderController.baseStatistics);

orderRouter.patch('/update-status/:orderId', orderController.updateStatus);

export default orderRouter;
