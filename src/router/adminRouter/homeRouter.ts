import express from 'express';
import HomeController from '../../controllers/admin/HomeController';

const homeController = HomeController.instance;
const homeRouter = express.Router();

homeRouter.post('/create', homeController.create);

export default homeRouter;
