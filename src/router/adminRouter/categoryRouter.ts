import express from 'express';
import CategoryController from '../../controllers/admin/CategoryController';

const categoryController = CategoryController.instance;
const categoryRouter: express.Router = express.Router();

categoryRouter.get('/list', categoryController.list);
categoryRouter.get('/find/:id', categoryController.find);

categoryRouter.post('/create', categoryController.create);

categoryRouter.patch('/update/:id', categoryController.update);

categoryRouter.delete('/remove/:id', categoryController.remove);
categoryRouter.delete('/removeAll/:id', categoryController.removeAll);

export default categoryRouter;
