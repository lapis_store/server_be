import express from 'express';
import ProductDetailController from '../../controllers/admin/ProductDetailController';

const productDetailController = ProductDetailController.instance;
const productDetailRouter = express.Router();

productDetailRouter.post('/create', productDetailController.create);
productDetailRouter.patch('/update', productDetailController.update);
productDetailRouter.get('/find/:id', productDetailController.find);

export default productDetailRouter;
