import express from 'express';
import ProductController from '../../controllers/admin/ProductController';

const productController = ProductController.instance;
const productRouter = express.Router();

productRouter.post('/create', productController.create);
productRouter.patch('/update/:id', productController.update);
productRouter.delete('/remove/:id', productController.remove);
productRouter.get('/find/:id', productController.find);
productRouter.get('/list', productController.list);
productRouter.get('/check-slug', productController.checkSlug);
productRouter.get('/image-name/:id', productController.imageName);

export default productRouter;
