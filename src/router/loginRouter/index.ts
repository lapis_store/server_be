import * as express from 'express';
import adminLoginRouter from './adminLoginRouter';
const loginRouter = express.Router();

loginRouter.use('/admin-login', adminLoginRouter);

export default loginRouter;
