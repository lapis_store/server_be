import * as express from 'express';
import AdminLoginController from '../../controllers/login/AdminLoginController';
import acceptedRequest from '../../functions/acceptedRequest';

const adminLoginController = AdminLoginController.instance;
const adminLoginRouter = express.Router();

adminLoginRouter.head('/sign-in', acceptedRequest);
adminLoginRouter.options('/sign-in', acceptedRequest);
adminLoginRouter.post('/sign-in', adminLoginController.signIn);

export default adminLoginRouter;
