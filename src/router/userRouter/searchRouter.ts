import express from 'express';
import SearchController from '../../controllers/user/SearchController/SearchController';

const searchController = SearchController.instance;
const searchRouter = express.Router();

searchRouter.get('/', searchController.index);
searchRouter.get('/suggestion', searchController.suggestions);

export default searchRouter;
