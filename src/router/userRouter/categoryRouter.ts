import express from 'express';
import CategoryController from '../../controllers/user/CategoryController/CategoryController';

const categoryController = CategoryController.instance;
const categoryRouter = express.Router();

categoryRouter.get('/list', categoryController.list);
categoryRouter.get('/find/:id', categoryController.find);

export default categoryRouter;
