import express from 'express';
import ProductController from '../../controllers/user/ProductController/ProductController';

const productController = ProductController.instance;
const productRouter = express.Router();

productRouter.get('/latest-products', productController.latestProducts);
productRouter.get('/list', productController.list);
productRouter.get('/get-by-id/:id', productController.getById);
productRouter.get('/get-by-list-id', productController.getByListId);
productRouter.get('/get-by-slug', productController.getBySlug);

export default productRouter;
