import express from 'express';

import footerController from '../../controllers/user/footerController';

const footerRouter = express.Router();
footerRouter.get('/list', footerController.list);

export default footerRouter;
