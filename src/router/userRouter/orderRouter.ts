import express from 'express';
import OrderController from '../../controllers/user/OrderController';
import acceptedRequest from '../../functions/acceptedRequest';

const orderController = OrderController.instance;
const orderRouter = express.Router();

orderRouter.head('/add', acceptedRequest);
orderRouter.options('/add', acceptedRequest);
orderRouter.post('/add', orderController.add);

export default orderRouter;
