import express from 'express';
import categoryRouter from './categoryRouter';
import footerRouter from './footerRouter';
import orderRouter from './orderRouter';
import productRouter from './productRouter';
import searchRouter from './searchRouter';

const userRouter = express.Router();

userRouter.use('/footer', footerRouter);
userRouter.use('/categories', categoryRouter);
userRouter.use('/product', productRouter);
userRouter.use('/search', searchRouter);
userRouter.use('/order', orderRouter);
export default userRouter;
