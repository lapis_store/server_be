import bodyParser from 'body-parser';
import express, { Application, Request, Response } from 'express';

import adminRouter from './adminRouter';
import accessControlAllow from '../middleware/accessControlAllow';
import path from 'path';
import userRouter from './userRouter';
import loginRouter from './loginRouter';
import cookieParser from 'cookie-parser';
import authenticateToken from '../middleware/authenticateToken';
import morgan from 'morgan';
import ENV from '../core/ENV';

function router(app: Application) {
    if (ENV.IS_DEV) {
        // app.use(morgan('combined'));
    }

    // access control
    app.use(accessControlAllow);

    // static file
    app.use('/static', express.static(path.join(__dirname, '../../public')));

    //
    app.use(cookieParser());

    //
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    //
    app.use('/admin', authenticateToken, adminRouter);
    app.use('/user', userRouter);
    app.use('/login', loginRouter);
}

export default router;
