import './core/configEnv';
import express, { Application, NextFunction, Request, Response } from 'express';
import lapisLog, { ELogColor } from './core/lapisLog';
import router from './router';
import ENV from './core/ENV';
import dbConnect from './core/dbConnect';

(async () => {
    const app: Application = express();

    if ((await Promise.all([dbConnect()])).includes(false)) {
        lapisLog('ERROR', `Some process failed when startup`);
        return;
    }
    await dbConnect();
    router(app);

    app.set('trust proxy', 1);

    app.get('/', (req: Request, res: Response) => {
        res.send('ok');
        res.end();
    });

    app.listen(ENV.PORT, () => {
        lapisLog('INFO', `Server running on ${ELogColor.FgBlue}http://127.0.0.1:${ENV.PORT}/${ELogColor.Reset}`);
    });
})();
